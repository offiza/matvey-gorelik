﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Calendar
{
	public partial class MainWindow : Window
	{
		MounthViewModel mounthViewModel = new MounthViewModel();
		SolidColorBrush red;
		SolidColorBrush green;
		SolidColorBrush blue;
		public MainWindow()
		{
			InitializeComponent();
			red = new SolidColorBrush(Color.FromRgb(255, 0, 0));
			green = new SolidColorBrush(Color.FromRgb(0, 255, 0));
			blue = new SolidColorBrush(Color.FromRgb(0, 0, 255));
		}

		private void radioButtonDecember_Checked(object sender, RoutedEventArgs e)
		{
			mounthViewModel.Mounth = new List<MyWeek>();

			mounthViewModel = new MounthViewModel(12);

			MounthList.ItemsSource = mounthViewModel.Mounth;
		}

		private void radioButtonJanuary_Checked(object sender, RoutedEventArgs e)
		{
			mounthViewModel.Mounth = new List<MyWeek>();

			mounthViewModel = new MounthViewModel(1);
			
			if(MounthList != null)
				MounthList.ItemsSource = mounthViewModel.Mounth;
		}

		private void radioButtonFebruary_Checked(object sender, RoutedEventArgs e)
		{
			mounthViewModel.Mounth = new List<MyWeek>();

			mounthViewModel = new MounthViewModel(2);

			MounthList.ItemsSource = mounthViewModel.Mounth;
		}

		private void radioButtonMarch_Checked(object sender, RoutedEventArgs e)
		{
			mounthViewModel.Mounth = new List<MyWeek>();

			mounthViewModel = new MounthViewModel(3);

			MounthList.ItemsSource = mounthViewModel.Mounth;
		}

		private void radioButtonApril_Checked(object sender, RoutedEventArgs e)
		{
			mounthViewModel.Mounth = new List<MyWeek>();

			mounthViewModel = new MounthViewModel(4);

			MounthList.ItemsSource = mounthViewModel.Mounth;
		}

		private void radioButtonMay_Checked(object sender, RoutedEventArgs e)
		{
			mounthViewModel.Mounth = new List<MyWeek>();

			mounthViewModel = new MounthViewModel(5);

			MounthList.ItemsSource = mounthViewModel.Mounth;
		}

		private void radioButtonJune_Checked(object sender, RoutedEventArgs e)
		{
			mounthViewModel.Mounth = new List<MyWeek>();

			mounthViewModel = new MounthViewModel(6);

			MounthList.ItemsSource = mounthViewModel.Mounth;
		}

		private void radioButtonJuly_Checked(object sender, RoutedEventArgs e)
		{
			mounthViewModel.Mounth = new List<MyWeek>();

			mounthViewModel = new MounthViewModel(7);

			MounthList.ItemsSource = mounthViewModel.Mounth;
		}

		private void radioButtonAugust_Checked(object sender, RoutedEventArgs e)
		{
			mounthViewModel.Mounth = new List<MyWeek>();

			mounthViewModel = new MounthViewModel(8);

			MounthList.ItemsSource = mounthViewModel.Mounth;
		}

		private void radioButtonSeptember_Checked(object sender, RoutedEventArgs e)
		{
			mounthViewModel.Mounth = new List<MyWeek>();

			mounthViewModel = new MounthViewModel(9);

			MounthList.ItemsSource = mounthViewModel.Mounth;
		}

		private void radioButtonOctober_Checked(object sender, RoutedEventArgs e)
		{
			mounthViewModel.Mounth = new List<MyWeek>();

			mounthViewModel = new MounthViewModel(10);

			MounthList.ItemsSource = mounthViewModel.Mounth;
		}

		private void radioButtonNovember_Checked(object sender, RoutedEventArgs e)
		{
			mounthViewModel.Mounth = new List<MyWeek>();

			mounthViewModel = new MounthViewModel(11);

			MounthList.ItemsSource = mounthViewModel.Mounth;
		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			var button = (Button)sender;


			if (button.Background == red)
			{
				button.Background = green;
				return;
			}
			else if (button.Background == green)
			{
				button.Background = blue;
				return;
			}
			else
			{
				button.Background = red;
			}

		}
	}
}
