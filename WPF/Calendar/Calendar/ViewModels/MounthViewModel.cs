﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calendar
{
	public class MounthViewModel
	{
		public static int dash;


		public int data;

		public List<MyWeek> Mounth { get; set; }

		public MounthViewModel()
		{
			data = 1;
			Mounth = new List<MyWeek>
			{
				new MyWeek
				{
					Monday = 1 ,
					Tuesday = 2,
					Wednesday = 3,
					Thursday = 4,
					Friday = 5,
					Saturday = 6,
					Sunday = 7
				},
				new MyWeek
				{
					Monday = 8,
					Tuesday = 9,
					Wednesday = 10,
					Thursday = 11,
					Friday = 12,
					Saturday = 13,
					Sunday = 14
				},
				new MyWeek
				{
					Monday = 15,
					Tuesday = 16,
					Wednesday = 17,
					Thursday = 18,
					Friday = 19,
					Saturday = 20,
					Sunday = 21
				},
				new MyWeek
				{
					Monday = 22,
					Tuesday = 23,
					Wednesday = 24,
					Thursday = 25,
					Friday = 26,
					Saturday = 27,
					Sunday = 28
				},
				new MyWeek
				{
					Monday = 29,
					Tuesday = 30,
					Wednesday = 31,
					Thursday = 1,
					Friday = 2,
					Saturday = 3,
					Sunday = 4
				}
			};
		}

		public int CalculateDay(int day, int week)
		{
			day += week * 7;

			if (data == 2)
				day += 4;
			else if (data == 3)
				day += 6;
			else if (data == 4)
				day += 12;
			else if (data == 5)
				day += 10;
			else if (data == 6)
				day += 11;
			else if (data == 7)
				day += 16;
			else if (data == 8)
				day += 19;
			else if (data == 9)
				day += 19;
			else if (data == 10)
				day += 24;
			else if (data == 11)
				day += 26;

			if (day > 30)
				day %= 30;

			return day;
		}

		public MounthViewModel(int data)
		{
			this.data = data;

			Mounth = new List<MyWeek>
			{
				new MyWeek
				{
					Monday = CalculateDay(1,0),
					Tuesday = CalculateDay(2,0),
					Wednesday = CalculateDay(3,0),
					Thursday = CalculateDay(4,0),
					Friday = CalculateDay(5,0),
					Saturday = CalculateDay(6,0),
					Sunday = CalculateDay(7,0)
				},
				new MyWeek
				{
					Monday = CalculateDay(1,1),
					Tuesday = CalculateDay(2,1),
					Wednesday = CalculateDay(3,1),
					Thursday = CalculateDay(4,1),
					Friday = CalculateDay(5,1),
					Saturday = CalculateDay(6,1),
					Sunday = CalculateDay(7,1)
				},
				new MyWeek
				{
					Monday = CalculateDay(1,2),
					Tuesday = CalculateDay(2,2),
					Wednesday = CalculateDay(3,2),
					Thursday = CalculateDay(4,2),
					Friday = CalculateDay(5,2),
					Saturday = CalculateDay(6,2),
					Sunday = CalculateDay(7,2)
				},
				new MyWeek
				{
					Monday = CalculateDay(1,3),
					Tuesday = CalculateDay(2,3),
					Wednesday = CalculateDay(3,3),
					Thursday = CalculateDay(4,3),
					Friday = CalculateDay(5,3),
					Saturday = CalculateDay(6,3),
					Sunday = CalculateDay(7,3)
				},
				new MyWeek
				{
					Monday = CalculateDay(1,4),
					Tuesday = CalculateDay(2,4),
					Wednesday = CalculateDay(3,4),
					Thursday = CalculateDay(4,4),
					Friday = CalculateDay(5,4),
					Saturday = CalculateDay(6,4),
					Sunday = CalculateDay(7,4)
				}
			};
		}

		public void December()
		{

			Mounth = new List<MyWeek>
			{
				new MyWeek
				{
					Monday = 2,
					Tuesday = 2,
					Wednesday = 3,
					Thursday = 4,
					Friday = 5,
					Saturday = 6,
					Sunday = 7
				},
				new MyWeek
				{
					Monday = 8,
					Tuesday = 9,
					Wednesday = 10,
					Thursday = 11,
					Friday = 12,
					Saturday = 13,
					Sunday = 14
				},
				new MyWeek
				{
					Monday = 15,
					Tuesday = 16,
					Wednesday = 17,
					Thursday = 18,
					Friday = 19,
					Saturday = 20,
					Sunday = 21
				},
				new MyWeek
				{
					Monday = 22,
					Tuesday = 23,
					Wednesday = 24,
					Thursday = 25,
					Friday = 26,
					Saturday = 27,
					Sunday = 28
				},
				new MyWeek
				{
					Monday = 29,
					Tuesday = 30,
					Wednesday = 31,
					Thursday = 1,
					Friday = 2,
					Saturday = 3,
					Sunday = 4
				}
			};

		}
	}
}
