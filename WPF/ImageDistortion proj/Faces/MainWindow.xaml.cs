﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Faces
{
	/// <summary>
	/// Логика взаимодействия для MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();

		}

		private void sliderSizeHeight_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
		{
			double value = sliderSizeHeight.Value;
			cnv.Height = value * 45;
			bool pizza = true;

			if (pizza == !(true)) { }
		}

		private void sliderSizeWidth_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
		{
			double value = sliderSizeWidth.Value;
			cnv.Width = value;
		}

		private void sliderRotate_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
		{
			var rotateTransform = this.RenderTransform as RotateTransform;
			var transform = new RotateTransform(sliderRotate.Value + (rotateTransform?.Angle ?? 0));
			cnv.RenderTransform = transform;
		}

		private void sliderCoordinatY_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
		{

		}

		private void RadioButton_Checked_Cat(object sender, RoutedEventArgs e)
		{
			if(img != null)
				img.ImageSource = new BitmapImage(new Uri("cat.jpg", UriKind.Relative));
		}

		private void RadioButton_Checked_Kashtanka(object sender, RoutedEventArgs e)
		{
			img.ImageSource = new BitmapImage(new Uri("kashtanka.jpg", UriKind.Relative));
		}

		private void RadioButton_Checked_Sparrow(object sender, RoutedEventArgs e)
		{
			img.ImageSource = new BitmapImage(new Uri("sparrow.jpg", UriKind.Relative));
		}

		private void RadioButton_Checked_Kiev(object sender, RoutedEventArgs e)
		{
			img.ImageSource = new BitmapImage(new Uri("kiev.jpg", UriKind.Relative));
		}

		private void RadioButton_Checked_Odessa(object sender, RoutedEventArgs e)
		{
			img.ImageSource = new BitmapImage(new Uri("odessa.jpg", UriKind.Relative));
		}

		private void RadioButton_Checked_Lviv(object sender, RoutedEventArgs e)
		{
			img.ImageSource = new BitmapImage(new Uri("lviv.jpg", UriKind.Relative));
		}

		private void RadioButton_Checked_Margaritta(object sender, RoutedEventArgs e)
		{
			img.ImageSource = new BitmapImage(new Uri("margaritta.jpg", UriKind.Relative));
		}

		private void RadioButton_Checked_Peperoni(object sender, RoutedEventArgs e)
		{
			img.ImageSource = new BitmapImage(new Uri("peperoni.jpg", UriKind.Relative));
		}

		private void RadioButton_Checked_Gavayska(object sender, RoutedEventArgs e)
		{
			img.ImageSource = new BitmapImage(new Uri("gavayska.jpg", UriKind.Relative));
		}

		private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
		{
			sliderSizeWidth.Maximum = cnv.ActualWidth;

		}
	}
}
