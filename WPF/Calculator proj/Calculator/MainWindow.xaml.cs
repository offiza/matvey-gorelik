﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Calculator
{
	/// <summary>
	/// Логика взаимодействия для MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		string sign = String.Empty;
		string data = "0";
		double temp = 0;
		string result = "0";

		bool ender = false;
		public MainWindow()
		{
			InitializeComponent();
			labelResult.Content = result;
			labelData.Content = data + sign;
		}

		private void buttonNum_Click(object sender, RoutedEventArgs e)
		{
			if (result == data)
				result = "0";

			if (ender)
			{
				result = "0";
				ender = !ender;
			}

			if (result == "0")
			{
				if (sender == button1)
					result = button1.Content.ToString();
				else if (sender == button2)
					result = button2.Content.ToString();
				else if (sender == button3)
					result = button3.Content.ToString();
				else if (sender == button4)
					result = button4.Content.ToString();
				else if (sender == button5)
					result = button5.Content.ToString();
				else if (sender == button6)
					result = button6.Content.ToString();
				else if (sender == button7)
					result = button7.Content.ToString();
				else if (sender == button8)
					result = button8.Content.ToString();
				else if (sender == button9)
					result = button9.Content.ToString();
				else if (sender == button0)
					result = button0.Content.ToString();
			}
			else
			{
				if (sender == button1)
					result = result + button1.Content.ToString();
				else if (sender == button2)
					result = result + button2.Content.ToString();
				else if (sender == button3)
					result = result + button3.Content.ToString();
				else if (sender == button4)
					result = result + button4.Content.ToString();
				else if (sender == button5)
					result = result + button5.Content.ToString();
				else if (sender == button6)
					result = result + button6.Content.ToString();
				else if (sender == button7)
					result = result + button7.Content.ToString();
				else if (sender == button8)
					result = result + button8.Content.ToString();
				else if (sender == button9)
					result = result + button9.Content.ToString();
				else if (sender == button0)
					result = result + button0.Content.ToString();
			}

			labelResult.Content = result;
		}

		private void buttonSign_Click(object sender, RoutedEventArgs e)
		{
			if (data != "0")
			{
				if (sign == "+")
					temp += Convert.ToDouble(result);
				else if (sign == "-")
					temp -= Convert.ToDouble(result);
				else if (sign == "*")
					temp *= Convert.ToDouble(result);
				else if (sign == "/")
					temp /= Convert.ToDouble(result);
			}


			if (data == "0")
				temp += Convert.ToDouble(result);

			if (sender == buttonPlus)
				sign = "+";
			else if (sender == buttonMinus)
				sign = "-";
			else if (sender == buttonMultiply)
				sign = "x";
			else if (sender == buttonDivision)
				sign = "/";

			data = result;
			labelData.Content = labelData.Content + data + sign;
			labelResult.Content = result;
		}

		private void buttonEqual_Click(object sender, RoutedEventArgs e)
		{
			if (result != "" && data != "" && sign != "")
			{
				labelData.Content += result;

				if (sign == "+")
					temp += Convert.ToDouble(result);
				if (sign == "-")
					temp -= Convert.ToDouble(result);
				if (sign == "*")
					temp *= Convert.ToDouble(result);
				if (sign == "/")
					temp /= Convert.ToDouble(result);

				//result = Converted(result, sign, temp.ToString());

				result = temp.ToString();
				labelResult.Content = result;
				data = "";
				sign = "";
				ender = true;
			}
			else
			{

			}
		}

		//private string Converted(string r, string s, string d)
		//{
		//	double result = Convert.ToDouble(r);
		//	double data = Convert.ToDouble(d);
		//	if (s == "+")
		//		result = data + result;
		//	else if (s == "-")
		//		result = data - result;
		//	else if (s == "x")
		//		result = data * result;
		//	else if (s == "/")
		//		result = data / result;

		//	return result.ToString();
		//}

		private void ButtonClear_Click(object sender, RoutedEventArgs e)
		{
			result = "0";
			data = "";

			labelResult.Content = result;
			labelData.Content = data;
		}

		private void buttonPlus_Minus_Click(object sender, RoutedEventArgs e)
		{
			result = (Convert.ToInt32(result) * -1).ToString();
			labelResult.Content = result;
		}

		private void ButtonClearResut_Click(object sender, RoutedEventArgs e)
		{
			result = "0";
			labelResult.Content = result;
		}

		private void button1Dx_Click(object sender, RoutedEventArgs e)
		{
			if (result == "0") { }
			else
			{
				result = (1 / Convert.ToDouble(result)).ToString();
				labelResult.Content = result;
			}
		}

		private void buttonSqare_Click(object sender, RoutedEventArgs e)
		{
			if (result == "0") { }
			else
			{
				result = (Convert.ToDouble(result) * Convert.ToDouble(result)).ToString();
				labelResult.Content = result;
			}
		}

		private void buttonKoma_Click(object sender, RoutedEventArgs e)
		{
			if (result.Substring(result.Length - 1, 1) != ",")
			{
				result += ",";
			}
			if (ender)
				result = "0,";

			labelResult.Content = result;
		}

		private void Window_SizeChanged(object sender, RoutedEventArgs e)
		{
			if (Height < 400)
			{
				labelResult.FontSize = 20;
			}
			else if (Height < 800)
			{
				labelResult.FontSize = 34;
			}
			else if (Height < 1300)
			{
				labelResult.FontSize = 54;
			}
		}

		private void buttonBackcpace_Click(object sender, RoutedEventArgs e)
		{
			result = result.Substring(0, result.Length - 1);
			if (result == "")
			{
				result = "0";
			}
			labelResult.Content = result;
		}
	}
}
