﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
namespace PhotoRobot
{
	/// <summary>
	/// Логика взаимодействия для MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();
		}

		private void Ears_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			ComboBox comboBox = (ComboBox)sender;

			ComboBoxItem selectedItem = (ComboBoxItem)comboBox.SelectedItem;

			if (selectedItem.Content != null)
			{
				StackPanel stackPanel = (StackPanel)selectedItem.Content;

				Image selectedImage = (Image)stackPanel.Children[0];

				imgRightEar.Source = new BitmapImage(new Uri(selectedImage.Source.ToString()));
				imgLeftEar.Source = new BitmapImage(new Uri(selectedImage.Source.ToString()));
			}
		}

		private void Eyes_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			ComboBox comboBox = (ComboBox)sender;

			ComboBoxItem selectedItem = (ComboBoxItem)comboBox.SelectedItem;

			if (selectedItem.Content != null)
			{
				StackPanel stackPanel = (StackPanel)selectedItem.Content;

				Image selectedImage = (Image)stackPanel.Children[0];

				imgEyes.Source = new BitmapImage(new Uri(selectedImage.Source.ToString()));
			}
		}

		private void Mouth_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			ComboBox comboBox = (ComboBox)sender;

			ComboBoxItem selectedItem = (ComboBoxItem)comboBox.SelectedItem;

			if (selectedItem.Content != null)
			{
				StackPanel stackPanel = (StackPanel)selectedItem.Content;

				Image selectedImage = (Image)stackPanel.Children[0];

				imgMouth.Source = new BitmapImage(new Uri(selectedImage.Source.ToString()));
			}
		}

		private void Nose_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			ComboBox comboBox = (ComboBox)sender;

			ComboBoxItem selectedItem = (ComboBoxItem)comboBox.SelectedItem;

			if (selectedItem.Content != null)
			{
				StackPanel stackPanel = (StackPanel)selectedItem.Content;

				Image selectedImage = (Image)stackPanel.Children[0];

				imgNose.Source = new BitmapImage(new Uri(selectedImage.Source.ToString()));
			}
		}


		private void comboBoxHair_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			ComboBox comboBox = (ComboBox)sender;

			ComboBoxItem selectedItem = (ComboBoxItem)comboBox.SelectedItem;

			if (selectedItem.Content != null)
			{
				StackPanel stackPanel = (StackPanel)selectedItem.Content;

				Image selectedImage = (Image)stackPanel.Children[0];

				imgHair.Source = new BitmapImage(new Uri(selectedImage.Source.ToString()));
			}
		}

		private void Window_Loaded(object sender, RoutedEventArgs e)
		{
			string[] filePaths = Directory.GetFiles("./", "*.txt");

			foreach (var file in filePaths)
			{
				Saves.Items.Add(file.Replace("./", "").Replace(".txt", ""));
			}
		}

		private void buttonSave_Click(object sender, RoutedEventArgs e)
		{
			TextWriter textWriter = new StreamWriter(textBoxName.Text + ".txt");

			textWriter.WriteLine(imgLeftEar.Source.ToString());
			textWriter.WriteLine(imgRightEar.Source.ToString());
			textWriter.WriteLine(imgEyes.Source.ToString());
			textWriter.WriteLine(imgNose.Source.ToString());
			textWriter.WriteLine(imgMouth.Source.ToString());
			textWriter.WriteLine(imgHair.Source.ToString());

			textWriter.Close();

		}

		private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			var listbox = (ListBox)sender;

			TextReader textReader = new StreamReader(listbox.SelectedItem.ToString() + ".txt");

			imgLeftEar.Source = new BitmapImage(new Uri(textReader.ReadLine()));

			imgRightEar.Source = new BitmapImage(new Uri(textReader.ReadLine()));

			imgEyes.Source = new BitmapImage(new Uri(textReader.ReadLine()));

			imgNose.Source = new BitmapImage(new Uri(textReader.ReadLine()));

			imgMouth.Source = new BitmapImage(new Uri(textReader.ReadLine()));

			imgHair.Source = new BitmapImage(new Uri(textReader.ReadLine()));

			textReader.Close();
		}
	}
}
