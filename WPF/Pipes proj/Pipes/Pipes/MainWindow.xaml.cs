﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Threading;

namespace Pipes
{
	public abstract class AbstractButton : Button
	{
		public double Angle { get; set; }

		public AbstractButton()
		{
			Angle = 0;
		}

		public abstract void Rotate();

		public abstract void Rotate(int rand);

		public abstract bool Check();

		public void ChangeColor()
		{
		}
	}

	public class AngleButton : AbstractButton
	{
		public AngleButton()
		{
			Angle = 0;
		}

		public override void Rotate()
		{
			var rotateTransform = this.RenderTransform as RotateTransform;
			var transform = new RotateTransform(90 + (rotateTransform?.Angle ?? 0));
			this.RenderTransform = transform;

			Angle = transform.Angle;

		}

		public override void Rotate(int rand)
		{
			var rotateTransform = this.RenderTransform as RotateTransform;
			var transform = new RotateTransform(90 * rand + (rotateTransform?.Angle ?? 0));
			this.RenderTransform = transform;
			
			Angle = transform.Angle;
		}

		public override bool Check() 
		{
			if (Angle == 0 || Angle % 360 == 0)
			{
				return true;
			}

			return false;
		}
	}

	public class StraightButton : AbstractButton
	{
		public StraightButton()
		{
			Angle = 0;
		}

		public override void Rotate()
		{
			var rotateTransform = this.RenderTransform as RotateTransform;
			var transform = new RotateTransform(90 + (rotateTransform?.Angle ?? 0));
			this.RenderTransform = transform;

			Angle = transform.Angle;

		}

		public override void Rotate(int rand)
		{
			var rotateTransform = this.RenderTransform as RotateTransform;
			var transform = new RotateTransform(90 * rand + (rotateTransform?.Angle ?? 0));
			this.RenderTransform = transform;

			Angle = transform.Angle;
		}

		public override bool Check()
		{
			if (Angle == 0 || Angle % 180 == 0)
			{
				return true;
			}

			return false;
		}
	}

	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();
		}

		private void button_start_Click(object sender, RoutedEventArgs e)
		{
			bool checkVictory = true;
			List<AngleButton> abbtns = new List<AngleButton>() { button0_1, button0_3, button1_0, button1_1, button1_2, button1_3 };
			List<StraightButton> strbtns = new List<StraightButton>() { button0_2, button2_2, button3_2, button4_2 };

			foreach (var btn in abbtns)
			{
				if (!btn.Check())
				{
					checkVictory = false;
				}
			}

			foreach (var btn in strbtns)
			{
				if (!btn.Check())
				{
					checkVictory = false;
				}
			}
			if (checkVictory)
			{
				imageButton1_0.Opacity = 1;
				imageButton1_1.Opacity = 1;
				imageButton0_1.Opacity = 1;
				imageButton0_2.Opacity = 1;
				imageButton0_3.Opacity = 1;
				imageButton1_3.Opacity = 1;
				imageButton1_2.Opacity = 1;
				imageButton2_2.Opacity = 1;
				imageButton3_2.Opacity = 1;
				imageButton4_2.Opacity = 1;
				imageButton5_2.Opacity = 1;
				MessageBox.Show("Hello, u won!");
			}
			else
			{
				MessageBox.Show("Hello, u lose!");
			}
		}

		private void button_quit_Click(object sender, RoutedEventArgs e)
		{
			Close();
			
		}

		private void ButtonClick(object sender, RoutedEventArgs e)
		{
			AngleButton btn = sender as AngleButton;

			btn.Rotate();
		}

		private void ButtonStraightClick(object sender, RoutedEventArgs e)
		{
			StraightButton btn = sender as StraightButton;

			btn.Rotate();
		}

		private void button_reset_Click(object sender, RoutedEventArgs e)
		{
			Random random = new Random();

			List<AbstractButton> btns = new List<AbstractButton>() { button0_1,button0_2,button0_3,button1_0,button1_1,button1_2,button1_3,button2_2,button3_2,button4_2};

			foreach (var btn in btns)
			{
				btn.Rotate(random.Next(1, 4));
			}

			imageButton1_0.Opacity = 0;
			imageButton1_1.Opacity = 0;
			imageButton0_1.Opacity = 0;
			imageButton0_2.Opacity = 0;
			imageButton0_3.Opacity = 0;
			imageButton1_3.Opacity = 0;
			imageButton1_2.Opacity = 0;
			imageButton2_2.Opacity = 0;
			imageButton3_2.Opacity = 0;
			imageButton4_2.Opacity = 0;
			imageButton5_2.Opacity = 0;
		}
	}
}
