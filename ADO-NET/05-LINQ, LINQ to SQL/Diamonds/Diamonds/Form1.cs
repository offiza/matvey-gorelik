﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;

namespace Diamonds
{
    public partial class Form1 : Form
    {
        XmlElement xRoot;
        XmlDocument xDoc;

        List<Gem> gems;

        public Form1()
        {
            InitializeComponent();

            xDoc = new XmlDocument();
            xDoc.Load("../../../Gem.xml");

            xRoot = xDoc.DocumentElement;

            gems = new List<Gem>();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            foreach (XmlElement xnode in xRoot)
            {
                Gem gem = new Gem();

                XmlNode attr = xnode.Attributes.GetNamedItem("name");

                if (attr != null)
                    gem.Name = attr.Value;

                foreach (XmlNode childnode in xnode.ChildNodes)
                {
                    if (childnode.Name == "color")
                        gem.Color = childnode.InnerText;
                    else if (childnode.Name == "transparency")
                        gem.Transparency = childnode.InnerText;
                    else
                        gem.Type = childnode.InnerText;
                }

                gems.Add(gem);
                comboBoxGem.Items.Add(gem.Name);

            }

            for (int i = 0; i < gems.Count; i+=2)
            {
                comboBoxColor.Items.Add(gems[i].Color);                
            }
        }

        private void comboBoxGem_SelectedIndexChanged(object sender, EventArgs e)
        {
            labelColor.Text = $"Color: {gems[comboBoxGem.SelectedIndex].Color}";
            labelTransparency.Text = $"Transparency: {gems[comboBoxGem.SelectedIndex].Transparency}";
            labelType.Text = $"Type: {gems[comboBoxGem.SelectedIndex].Type}";
        }

        private void comboBoxColor_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadGems(comboBoxColor.SelectedItem.ToString());
        }

        private void LoadGems(string color)
        {
            comboBoxGemColor.Items.Clear();

            XDocument xdoc = XDocument.Load("../../../Gem.xml");

            var items = from xe in xdoc.Element("gems").Elements("gem")
                        where (string)xe.Element("color").Value == color
                        select Name = xe.Attribute("name").Value;

            foreach(var gem in items)
            {
                comboBoxGemColor.Items.Add(gem);
            }
        }
    }
}
