﻿
namespace Diamonds
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBoxGem = new System.Windows.Forms.ComboBox();
            this.labelColor = new System.Windows.Forms.Label();
            this.labelTransparency = new System.Windows.Forms.Label();
            this.labelType = new System.Windows.Forms.Label();
            this.comboBoxColor = new System.Windows.Forms.ComboBox();
            this.comboBoxGemColor = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // comboBoxGem
            // 
            this.comboBoxGem.FormattingEnabled = true;
            this.comboBoxGem.Location = new System.Drawing.Point(0, 0);
            this.comboBoxGem.Name = "comboBoxGem";
            this.comboBoxGem.Size = new System.Drawing.Size(229, 28);
            this.comboBoxGem.TabIndex = 0;
            this.comboBoxGem.SelectedIndexChanged += new System.EventHandler(this.comboBoxGem_SelectedIndexChanged);
            // 
            // labelColor
            // 
            this.labelColor.AutoSize = true;
            this.labelColor.Location = new System.Drawing.Point(278, 31);
            this.labelColor.Name = "labelColor";
            this.labelColor.Size = new System.Drawing.Size(48, 20);
            this.labelColor.TabIndex = 1;
            this.labelColor.Text = "Color:";
            // 
            // labelTransparency
            // 
            this.labelTransparency.AutoSize = true;
            this.labelTransparency.Location = new System.Drawing.Point(278, 63);
            this.labelTransparency.Name = "labelTransparency";
            this.labelTransparency.Size = new System.Drawing.Size(98, 20);
            this.labelTransparency.TabIndex = 1;
            this.labelTransparency.Text = "Transparency:";
            // 
            // labelType
            // 
            this.labelType.AutoSize = true;
            this.labelType.Location = new System.Drawing.Point(278, 96);
            this.labelType.Name = "labelType";
            this.labelType.Size = new System.Drawing.Size(43, 20);
            this.labelType.TabIndex = 2;
            this.labelType.Text = "Type:";
            // 
            // comboBoxColor
            // 
            this.comboBoxColor.FormattingEnabled = true;
            this.comboBoxColor.Location = new System.Drawing.Point(0, 244);
            this.comboBoxColor.Name = "comboBoxColor";
            this.comboBoxColor.Size = new System.Drawing.Size(229, 28);
            this.comboBoxColor.TabIndex = 3;
            this.comboBoxColor.SelectedIndexChanged += new System.EventHandler(this.comboBoxColor_SelectedIndexChanged);
            // 
            // comboBoxGemColor
            // 
            this.comboBoxGemColor.FormattingEnabled = true;
            this.comboBoxGemColor.Location = new System.Drawing.Point(278, 244);
            this.comboBoxGemColor.Name = "comboBoxGemColor";
            this.comboBoxGemColor.Size = new System.Drawing.Size(229, 28);
            this.comboBoxGemColor.TabIndex = 3;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.comboBoxGemColor);
            this.Controls.Add(this.comboBoxColor);
            this.Controls.Add(this.labelType);
            this.Controls.Add(this.labelTransparency);
            this.Controls.Add(this.labelColor);
            this.Controls.Add(this.comboBoxGem);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxGem;
        private System.Windows.Forms.Label labelColor;
        private System.Windows.Forms.Label labelTransepasity;
        private System.Windows.Forms.Label labelTransparency;
        private System.Windows.Forms.Label labelType;
        private System.Windows.Forms.ComboBox comboBoxColor;
        private System.Windows.Forms.ComboBox comboBoxGemColor;
    }
}

