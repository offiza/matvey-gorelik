﻿
namespace Linq
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBoxWorkers = new System.Windows.Forms.ComboBox();
            this.richTextBoxDays = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // comboBoxWorkers
            // 
            this.comboBoxWorkers.FormattingEnabled = true;
            this.comboBoxWorkers.Location = new System.Drawing.Point(12, 12);
            this.comboBoxWorkers.Name = "comboBoxWorkers";
            this.comboBoxWorkers.Size = new System.Drawing.Size(358, 28);
            this.comboBoxWorkers.TabIndex = 0;
            this.comboBoxWorkers.SelectedIndexChanged += new System.EventHandler(this.comboBoxWorkers_SelectedIndexChanged);
            // 
            // richTextBoxDays
            // 
            this.richTextBoxDays.Location = new System.Drawing.Point(12, 46);
            this.richTextBoxDays.Name = "richTextBoxDays";
            this.richTextBoxDays.Size = new System.Drawing.Size(358, 376);
            this.richTextBoxDays.TabIndex = 1;
            this.richTextBoxDays.Text = "";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(382, 434);
            this.Controls.Add(this.richTextBoxDays);
            this.Controls.Add(this.comboBoxWorkers);
            this.Name = "MainForm";
            this.Text = "Shedule";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxWorkers;
        private System.Windows.Forms.RichTextBox richTextBoxDays;
    }
}

