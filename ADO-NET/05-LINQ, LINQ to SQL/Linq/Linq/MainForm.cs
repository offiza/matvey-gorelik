﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Linq
{
    public partial class MainForm : Form
    {
        private const string ConnectionString = "Data Source=DESKTOP-0701\\SQLEXPRESS2020;Initial Catalog=ScheduleDb;Integrated Security=SSPI;Pooling=False";
        private List<Worker> results;

        public MainForm()
        {
            InitializeComponent();

            results = new List<Worker>();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            string findWorkersQuery = $"Select * From Shedules";

            try
            {
                using (var connection = new SqlConnection(ConnectionString))
                {
                    connection.Open();

                    using (var sqlcommand = new SqlCommand(findWorkersQuery, connection))
                    using (SqlDataReader reader = sqlcommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            int id = reader.GetInt32("Id");
                            var Name = reader.GetString("Name");

                            results.Add(new Worker { Id = id, Name = Name });
                            comboBoxWorkers.Items.Add(results[results.Count - 1].Name);
                        }
                    }
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
        }

        private void comboBoxWorkers_SelectedIndexChanged(object sender, EventArgs e)
        {
            string findDateQuery = $"Select * From Shedules Where Id = {comboBoxWorkers.SelectedIndex + 1}";
            string findDaysQuery = $"Select * From DayOfWorks Where SheduleFk  = {comboBoxWorkers.SelectedIndex + 1}";

            richTextBoxDays.Clear();

            try
            {
                using (var connection = new SqlConnection(ConnectionString))
                {
                    connection.Open();

                    using (var sqlcommand = new SqlCommand(findDateQuery, connection))
                    using (SqlDataReader reader = sqlcommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            richTextBoxDays.Text += reader.GetDateTime("PeriodStart").ToString() + " - " + reader.GetDateTime("PeriodEnd").ToString();
                        }
                    }

                    connection.Close();

                    connection.Open();

                    using (var sqlcommandDays = new SqlCommand(findDaysQuery, connection))
                    using (SqlDataReader readerDay = sqlcommandDays.ExecuteReader())
                    {
                        while (readerDay.Read())
                        {
                            richTextBoxDays.Text += readerDay.GetInt32("Day").ToString();
                            richTextBoxDays.Text += "\n";
                        }
                    }

                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
        }
    }
}
