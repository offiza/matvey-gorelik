IF DB_ID('ScheduleDb') IS NOT NULL
BEGIN
	USE master
    ALTER DATABASE ScheduleDb SET SINGLE_USER WITH ROLLBACK IMMEDIATE;
    DROP DATABASE ScheduleDb;
END
GO

CREATE DATABASE ScheduleDb
GO

USE ScheduleDb
GO

/***********Creating tables*************/
CREATE TABLE [dbo].[Shedule](
	[Id] [int] NOT NULL IDENTITY(1,1) PRIMARY KEY,
	[Name] [nvarchar](32) NOT NULL,
	[PeriodStart] [date] NOT NULL,
	[PeriodEnd] [date] NOT NULL,

)
GO

CREATE TABLE [dbo].[DayOfWorks](
	[Id] [int] NOT NULL IDENTITY(1,1) PRIMARY KEY,
	[Day] [int] NOT NULL, 
	[SheduleFk] [int] NOT NULL,
)
GO

/***********Foreign Keys*************/

ALTER TABLE [dbo].[DayOfWorks] 
WITH CHECK ADD CONSTRAINT [FK_DayOfWorks_Shedule] FOREIGN KEY([SheduleFk])
REFERENCES [dbo].[Shedule] ([Id])
ON DELETE NO ACTION
ON UPDATE NO ACTION
GO

/***** Insert Queries *****/

INSERT INTO Shedule(Name, PeriodStart, PeriodEnd) VALUES ('������ ����', CAST('01-04-2021' AS DATE),  CAST('01-05-2021' AS DATE)), ('���� �����', CAST('01-04-2021' AS DATE),  CAST('01-05-2021' AS DATE))

INSERT INTO DayOfWorks(Day, SheduleFk) VALUES (1,1), (2,1),(5,1),(6,1),(9,1),(12,1), (3,2), (2,2),(4,2),(7,2),(8,2),(9,2),(10,2)
GO
