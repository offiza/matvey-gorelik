﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;

namespace PhoneBook
{
    public partial class Form1 : Form
    {
        private Timer timer = new Timer();
        private int timerCounter = 0;
        private bool isCall;


        public Form1()
        {
            InitializeComponent();

            isCall = false;
        }

        private void TimerTick(object sender, EventArgs e)
        {
            if (timerCounter < 9)
                labelTimer.Text = $"00:0{(++timerCounter)}";
            else if (timerCounter < 60)
                labelTimer.Text = $"00:{(++timerCounter)}";
        }

        private void buttonCall_Click(object sender, EventArgs e)
        {
            if (!isCall)
            {
                if (checkPhoneNumber())
                    Calling();
            }
            else
            {
                StopCalling();
            }
        }

        private bool checkPhoneNumber()
        {
            var regex = new Regex("^[0-9]+$");

            if (textBoxPhoneNumber.Text != string.Empty && regex.IsMatch(textBoxPhoneNumber.Text))
                return true;

            return false;
        }

        private void Calling()
        {
            isCall = true;

            timer = new Timer();
            timer.Interval = (1000);
            timer.Tick += new EventHandler(TimerTick);

            timer.Start();

            buttonCall.BackColor = Color.Red;
            buttonCall.Text = "End";
        }

        private void StopCalling()
        {
            isCall = false;

            timer.Stop();

            buttonCall.BackColor = Color.Green;
            buttonCall.Text = "Call";

            XDocument xdoc = new XDocument(new XElement("callings",
                new XElement("calling",
                    new XElement("number", $"{textBoxPhoneNumber.Text}"),
                    new XElement("talkTime", $"{timerCounter}"),
                    new XElement("time", $"{DateTime.Now}"))));
                 
            xdoc.Save("callings.xml"); 

            textBoxPhoneNumber.Text = string.Empty;
            labelTimer.Text = string.Empty;

            UpdateData();
        }


        private void UpdateData()
        {

            XDocument xdoc = XDocument.Load("callings.xml");

            foreach (XElement call in xdoc.Element("callings").Elements("calling"))
            {
                XElement number = call.Element("number");
                XElement talkTime = call.Element("talkTime");
                XElement time = call.Element("time");

                string[] param = { number.Value, talkTime.Value, time.Value };

                if (number != null && talkTime != null && time != null)
                {
                    dataGridView.Rows.Add(param);
                }
            }
        }

        private void buttonNumber_Click(object sender, EventArgs e)
        {
            UpdateData();

        }

        private void dataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
