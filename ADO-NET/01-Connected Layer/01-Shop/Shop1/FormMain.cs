﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shop1
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();

            SetCombobox();
        }

        public void SetCombobox()
        {
            string connectionString = "Data Source=DESKTOP-0701\\SQLEXPRESS2020;Initial Catalog=CandyShopDb;" +
                "Integrated Security=SSPI;Pooling=False"; ;

            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    string queryTables = "SELECT * FROM INFORMATION_SCHEMA.TABLES";

                    using (var command = new SqlCommand(queryTables, connection))
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                            comboBox.Items.Add($"{reader["TABLE_NAME"]}");
                    }

                }
            }
            catch (DbException ex)
            {
                MessageBox.Show("Problems with DataBase");
            }
        }

        private void comboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            string connectionString = "Data Source=DESKTOP-0701\\SQLEXPRESS2020;Initial Catalog=CandyShopDb;" +
                "Integrated Security=SSPI;Pooling=False";

            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    string queryTables = "";

                    if (comboBox.SelectedIndex == 0)
                    {
                        queryTables = "SELECT C.Name AS Name, C.SecondName AS SecondName FROM dbo.Customers C";
                    }
                    else if (comboBox.SelectedIndex == 1)
                    {
                        queryTables = "SELECT S.Name AS Name, S.SecondName AS SecondName FROM dbo.Sellers S";
                    }
                    else
                    {
                        queryTables = "SELECT D.Amount AS Amount, D.Date AS Date,C.Name AS Customer, S.Name AS Seller From Deals D,Sellers S, Customers C WHERE C.Id = D.CustomerFk AND S.Id = D.SellerFk";

                        using (var command = new SqlCommand(queryTables, connection))
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            richTextBox.Text = "Amount\tDate\tCustomer\tSeller\n\n";

                            while (reader.Read())
                                richTextBox.Text += ($"{reader["Amount"]}\t{reader["Date"]}\t{reader["Customer"]}\t{reader["Seller"]}\n");
                        }

                        return;
                    }

                    using (var command = new SqlCommand(queryTables, connection))
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        richTextBox.Text = "Name\tSecond Name\n\n";

                        while (reader.Read())
                            richTextBox.Text += ($"{reader["Name"]}\t{reader["SecondName"]}\n");
                    }
                }
            }
            catch (DbException ex)
            {
                MessageBox.Show("Problems with DataBase");
            }
        }
    }
}
