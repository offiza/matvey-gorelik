IF DB_ID('CandyShopDb') IS NOT NULL
BEGIN
	USE master
    ALTER DATABASE CandyShopDb SET SINGLE_USER WITH ROLLBACK IMMEDIATE;
    DROP DATABASE CandyShopDb;
END
GO

CREATE DATABASE CandyShopDb
GO

USE CandyShopDb
GO

/***********Creating tables*************/
CREATE TABLE [dbo].[Customers](
	[Id] [int] NOT NULL IDENTITY(1,1) PRIMARY KEY,
	[Name] [nvarchar](32) NOT NULL,
	[SecondName] [nvarchar](32) NOT NULL,
)
GO

CREATE TABLE [dbo].[Sellers](
	[Id] [int] NOT NULL IDENTITY(1,1) PRIMARY KEY,
	[Name] [nvarchar](32) NOT NULL,
	[SecondName] [nvarchar](32) NOT NULL,
)
GO

CREATE TABLE [dbo].[Deals](
	[Id] [int] NOT NULL IDENTITY(1,1) PRIMARY KEY,
	[Amount] [int] NOT NULL ,
	[Date] [int] NOT NULL,
	[CustomerFk] [int] NOT NULL,
	[SellerFk] [int] NOT NULL,
)
GO

/***********Foreign Keys*************/

ALTER TABLE [dbo].[Deals] 
WITH CHECK ADD CONSTRAINT [FK_Deal_Customer] FOREIGN KEY([CustomerFk])
REFERENCES [dbo].[Customers] ([Id])
ON DELETE NO ACTION
ON UPDATE NO ACTION
GO

ALTER TABLE [dbo].[Deals] 
WITH CHECK ADD CONSTRAINT [FK_Deal_Seller] FOREIGN KEY([SellerFk])
REFERENCES [dbo].[Customers] ([Id])
ON DELETE NO ACTION
ON UPDATE NO ACTION
GO

/***** Insert Queries *****/

INSERT INTO Customers(Name, SecondName) VALUES ('������', '����'), ('����', '����'), ('�����', '�����������')
INSERT INTO Sellers (Name, SecondName) VALUES ('������', '������'), ('�����', '�������'), ('������', '�������')

INSERT INTO Deals(Amount, Date, CustomerFk, SellerFk) VALUES (100,2010,1,2), (40,2011,1,2), (50,2010,3,2),(100,2010,2,2), (20,2010,2,1), (100,2010,1,2)

GO

*/*SELECT D.Amount AS Amount, D.Date AS Date,C.Name AS Customer, S.Name AS Seller From Deals D,Sellers S, Customers C WHERE C.Id = D.CustomerFk AND S.Id = D.SellerFk*/