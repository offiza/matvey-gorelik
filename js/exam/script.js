const canvas = document.getElementById('game');
const ctx = canvas.getContext('2d');

const imgBackground = new Image();
const imgApple = new Image();
const imgCorn = new Image();
const imgSomething = new Image();
const tileset = new Image();

const imgHead = new Image();
const imgBody = new Image();

const imgEnemy = new Image();

let interval = 200;
const intervalEdge = 50;
const intervalDecrement = 10;

let square = 32;
const MAP_WIDTH = 21;
const MAP_HEIGHT = 15;
let score = 0;
let direction = "up";
let map = createMap();

imgBackground.src = "img/background.jpg";
imgApple.src = "img/apple.png";
imgCorn.src = "img/corn.png";
imgSomething.src = "img/something.png";
tileset.src = "img/tileset.jpg"

imgHead.src = "img/head.png";
imgBody.src = "img/body.png";

imgEnemy.src = "img/stone.png";

ctx.font = '24px Verdana'
ctx.textBaseline = 'middle'
ctx.textAlign = 'center'
ctx.textStyle = 'bold'
ctx.fillStyle = 'white'

let food = {
    x: Math.floor(Math.random() * 20 + 1) * square,
    y: Math.floor(Math.random() * 13 + 2) * square,
    img: imgApple
}

let enemy = {
    x: Math.floor(Math.random() * 20 + 1) * square,
    y: Math.floor(Math.random() * 13 + 2) * square,
    img: imgEnemy
}

let foods = [imgApple, imgCorn, imgSomething];
let snake = [];


snake[0] = {
    x: 10 * square,
    y: 10 * square
};
snake[1] = {
    x: 10 * square,
    y: 10 * square
};
snake[2] = {
    x: 10 * square,
    y: 10 * square
};


document.addEventListener("keydown", (e) => {
    if (e.keyCode == 37 && direction != "right")
        direction = "left";
    else if (e.keyCode == 38 && direction != "down")
        direction = "up";
    else if (e.keyCode == 39 && direction != "left")
        direction = "right";
    else if (e.keyCode == 40 && direction != "up")
        direction = "down";
})

let snakeX = snake[0].x;
let snakeY = snake[0].y;

function Game() {
    Draw();
    Score();
    Move();
    Collision();
    CollisionEnemy();
}

let gameInterval = setInterval(Game, interval);

function Score() {
    ctx.fillText(score, square * 2, square * 1.5)
}

function Draw() {

    ctx.drawImage(imgBackground, 0, 0)

    for (let w = 0; w < MAP_WIDTH; w++) {
        for (let h = 0; h < MAP_HEIGHT; h++) {
            DrawGrassTile(map[w][h], w, h)
        }
    }

    ctx.drawImage(food.img, food.x, food.y)

    ctx.drawImage(imgHead, snake[0].x, snake[0].y)

    for (let i = 1; i < snake.length; i++) {
        ctx.drawImage(imgBody, snake[i].x, snake[i].y)
    }

    ctx.drawImage(enemy.img, enemy.x, enemy.y)
}


function DrawGrassTile(tileIndex, w, h) {
    ctx.drawImage(tileset, tileIndex * square - square, 0, square, square, (w * square) + 33, (h * square) + 62, square, square)
}

function createMap() {
    let arr = []
    for (let w = 0; w < MAP_WIDTH; w++) {
        arr.push([])
        for (let h = 0; h < MAP_HEIGHT; h++) {
            arr[w][h] = getRandomIntInclusive(0, 8);
        }
    }
    return arr;
}

function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function Move() {
    if (snakeX == food.x && snakeY == food.y) {
        score++;
        ChangeInterval();

        while (true) {
            let isContinue = false;

            food = {
                x: Math.floor(Math.random() * 20 + 1) * square,
                y: Math.floor(Math.random() * 13 + 3) * square,
                img: foods[Math.floor(Math.random() * 3)]
            }

            for (let i = 0; i < snake.length; i++) {
                if (snake[i].x == food.x && snake[i].y == food.y) {
                    isContinue = true;
                }
            }
            if (isContinue)
                continue;

            break;
        }

    } else {
        snake.pop();
    }

    if (direction == "left") {
        snakeX -= square;
        imgHead.src = "img/headLeft.png";
    }
    if (direction == "right") {
        snakeX += square;
        imgHead.src = "img/headRight.png";
    }
    if (direction == "up") {
        snakeY -= square;
        imgHead.src = "img/head.png";
    }
    if (direction == "down") {
        snakeY += square;
        imgHead.src = "img/headDown.png";
    }

    let head = {
        x: snakeX,
        y: snakeY
    };

    snake.unshift(head);
}

function Collision() {
    for (let i = 1; i < snake.length; i++) {
        if (snakeX == snake[i].x && snakeY == snake[i].y)
            Loose();
    }
    if (snakeX < 1 || snakeX > 21 * square || snakeY < 2 * square || snakeY > 16 * square) {
        Loose();
    }
}

function CollisionEnemy() {
    if (snakeX == enemy.x && snakeY == enemy.y)
        Loose();
}


function ChangeInterval() {
    if (interval > intervalEdge) {
        interval -= intervalDecrement
        clearInterval(gameInterval);
        gameInterval = setInterval(Game, interval);
    }
}

function Loose() {

    let textString = 'Ты проиграл! F5 для рестарта!';

    ctx.fillStyle = `rgba(0,0,0,0.3)`
    ctx.fillRect(0, 0, imgBackground.width, imgBackground.height);

    ctx.fillStyle = `white`
    ctx.fillText(textString, imgBackground.width / 2, imgBackground.height / 2)
    clearInterval(gameInterval);
}