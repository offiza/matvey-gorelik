function clockPainter() {
    let time = new Date();
    let sec = time.getSeconds();

    let ctx = document.getElementById("clock").getContext("2d");
    ctx.save();

    ctx.lineWidth = 4;

    ctx.clearRect(0, 0, 150, 150);
    ctx.translate(100, 100);
    ctx.scale(0.5, 0.5);

    ctx.save();
    ctx.beginPath();

    for (let i = 0; i < 60; i++) {
        ctx.rotate(Math.PI / 30);
        if (i % 5 == 1)
            ctx.moveTo(80, 0);
        else
            ctx.moveTo(100, 0);

        ctx.lineTo(120, 0);
    }

    ctx.stroke();
    ctx.restore();

    ctx.save();

    ctx.rotate(sec * Math.PI / 30);
    ctx.strokeStyle = "#000";
    ctx.lineWidth = 6;

    ctx.beginPath();
    ctx.moveTo(-30, 0);
    ctx.lineTo(83, 0);
    ctx.stroke();
    ctx.restore();

    ctx.restore();
}

window.onload = function() {
    setInterval(clockPainter, 1000);
}