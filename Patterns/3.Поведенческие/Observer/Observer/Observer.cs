﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Observer
{
	public partial class Observer : Form
	{
		public interface IObserver
		{
			void Update(ISubject subject, int x, int y);
		}
		public interface ISubject
		{
			void Add(IObserver observer);

			void Remove(IObserver observer);

		}
		public class CustomizeButton : Button, IObserver
		{
			public void Update(ISubject subject, int x, int y)
			{
				this.Location = new Point(Location.X + x, Location.Y + y);
			}
		}

		private class MainButton : Button, ISubject
		{
			private List<IObserver> observers = new List<IObserver>();

			public void Add(IObserver observer)
			{
				this.observers.Add(observer);
			}

			public void Remove(IObserver observer)
			{
				this.observers.Remove(observer);
			}
			public void MoveObj(int x, int y)
			{
				foreach (var item in observers)
				{
					item.Update(this, x, y);
				}
			}
		}

		private void ButtonOnClick(object sender, EventArgs eventArgs)
		{

		}
		public Observer()
		{
			InitializeComponent();
			UI();
		}
		private void UI()
		{
			MainButton buttonUp = new MainButton();
			MainButton buttonDown = new MainButton();
			MainButton buttonRight = new MainButton();
			MainButton buttonLeft = new MainButton();
			List < MainButton > mainList= new List<MainButton>();
			mainList.Add(buttonUp);
			mainList.Add(buttonDown);
			mainList.Add(buttonRight);
			mainList.Add(buttonLeft);
			int top = 10;
			int left = 10;
			for (int i = 0; i < 10; i++)
			{
				CustomizeButton button = new CustomizeButton();
				button.Left = left;
				button.Top = top;
				button.Name = "btn" + i;
				button.Click += ButtonOnClick;
				button.Text = "Moveble";
				button.BackColor = Color.White;
				this.Controls.Add(button);
				top += button.Height + 2;
			}
			void ButtonOnClick(object sender, EventArgs eventArgs)
			{				
				var button = (CustomizeButton)sender;
				if(button.BackColor != Color.BlueViolet)
				{
					button.BackColor = Color.BlueViolet;
					buttonUp.Add(button);
					buttonDown.Add(button);
				}
				else
				{
					button.BackColor = Color.White;
					buttonUp.Remove(button);
					buttonDown.Remove(button);
				}
			}
			buttonUp.Location = new Point(200, 50);
			buttonDown.Location = new Point(200, 80);
			buttonRight.Location = new Point(275, 80);
			buttonLeft.Location = new Point(125, 80);
			buttonUp.Text = "UP";
			buttonDown.Text = "DOWN";
			buttonRight.Text = "RIGHT";
			buttonLeft.Text = "LEFT";

			foreach (var item in mainList)
			{
				this.Controls.Add(item);
			}

			buttonUp.Click += ButtonUpClick;
			buttonDown.Click += ButtonDownClick;
			buttonRight.Click += ButtonRightClick;
			buttonLeft.Click += ButtonLeftClick;
			void ButtonUpClick(object sender, EventArgs eventArgs)
			{
				buttonUp.MoveObj(0, -3);
			}
			void ButtonDownClick(object sender, EventArgs eventArgs)
			{
				buttonDown.MoveObj(0, +3);
			}
			void ButtonRightClick(object sender, EventArgs eventArgs)
			{
				buttonUp.MoveObj(+3, 0);
			}
			void ButtonLeftClick(object sender, EventArgs eventArgs)
			{
				buttonDown.MoveObj(-3, 0);
			}
		}
	}
}
