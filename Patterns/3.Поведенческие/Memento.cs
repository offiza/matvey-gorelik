﻿using System;
using System.Collections.Generic;

namespace Memento
{
	
	public interface IOriginator
	{
		object GetMemento();
		void SetMemento(object _memento);
	}
	
	public class Notepad : IOriginator
	{
		private List<Caretaker> caretakers = new List<Caretaker>();
		private string Text { get; set; }
		public Notepad()
		{
			caretakers.Add(new Caretaker(this));
		}

		object IOriginator.GetMemento()
		{
			return new Memento { text = this.Text };
		}

		void IOriginator.SetMemento(object _memento)
		{
			if (Object.ReferenceEquals(_memento, null))
				throw new ArgumentNullException("memento");
			if (!(_memento is Memento))
				throw new ArgumentException("memento");
			Text = ((Memento)_memento).text;
		}

		public void AddText(string text)
		{
			Text += text;
			caretakers.Add(new Caretaker(this));
		}

		public void Undo()
		{
			if (caretakers.Count >= 2)
			{
				caretakers[caretakers.Count - 2].RestoreState(this);
				caretakers.RemoveAt(caretakers.Count - 1);
			}
		}

		public string GetText()
		{
			return Text;
		}
		class Memento
		{
			public string text { get; set; }
		}
	}

	public class Caretaker
	{
		private object Memento;
		public Caretaker(IOriginator originator)
		{
			SaveState(originator);
		}

		public void RestoreState(IOriginator originator)
		{
			if (originator == null)
				throw new Exception("originator");
			if (Memento == null)
				throw new Exception("_memento == null");
			originator.SetMemento(Memento);
		}

		public void SaveState(IOriginator originator)
		{
			if (originator != null)
				Memento = originator.GetMemento();
			else throw new Exception("originator");
		}
	}

	class Program
	{
		static void Main(string[] args)
		{
			bool isOn = true;
			Notepad note = new Notepad();

			while (isOn)
			{
				Console.WriteLine($"{note.GetText()}");
				var action = Console.ReadKey();
				if (action.Key == ConsoleKey.U)
					note.Undo();			
				else
				{
					var newText = Console.ReadLine();
					if (newText != null)
						note.AddText(newText);
				}
				Console.Clear();
			}
		}
	}
}

