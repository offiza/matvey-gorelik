﻿using System;
using System.Collections.Generic;

namespace Command
{
	public abstract class ICommand
	{
		public abstract void Execute();
		public abstract void Undo();
	}
	public class BoardCommand : ICommand
	{
		private Board board;
		private int x, y;
		public BoardCommand(int x, int y, Board board)
		{
			this.x = x;
			this.y = y;
			this.board = board;
		}
		public override void Execute()
		{
			board.BoardMass[x, y] = board.CurrentPlayer;
			board.Update();
			board.SwitchPlayer();
		}
		public override void Undo()
		{
			board.Undo();
		}
	}

	public class Board
	{
		public List<int[,]> boardStates = new List<int[,]>();

		public int CurrentPlayer;
		public bool IsRunning { get; set; }

		public int[,] BoardMass { get; set; }
		public Board()
		{
			BoardMass = new int[3, 3]
			{
				{0,0,0},
				{0,0,0},
				{0,0,0},
			};
			boardStates.Add((int[,])BoardMass.Clone());

			IsRunning = true;
			CurrentPlayer = 1;

			Update();
		}
		public bool Turn(int x, int y)
		{
			if (x > 2 || x < 0 || y > 2 || y < 0) 
				return false; 

			if (BoardMass[x, y] == 0)
			{
				ICommand command = new BoardCommand(x, y, this);
				command.Execute();

				boardStates.Add((int[,])BoardMass.Clone());
				return true;
			}
			else
			{
				Update();
				return false;
			}
		}
		public void Undo()
		{
			if (boardStates.Count >= 2)
			{
				BoardMass = boardStates[boardStates.Count - 2];
				boardStates.RemoveAt(boardStates.Count - 1);
				Update();
			}
		}
		public void Update()
		{
			Console.Clear();

			Console.WriteLine($"Игрок {CurrentPlayer}\n");
			for (int i = 0; i < 3; i++)
			{
				for (int j = 0; j < 3; j++)
				{
					Console.Write(BoardMass[i, j] + " ");
				}
				Console.WriteLine();
			}
			CheckWin();
			Console.WriteLine("\n");
		}
		public void SwitchPlayer()
		{
			if (CurrentPlayer == 1)
				CurrentPlayer = 2;
			else
				CurrentPlayer = 1;
		}
		public void CheckWin()
		{
			int val = 0;
			for (int i = 0; i < 3; i++)
			{
				if (BoardMass[i, val] == BoardMass[i, val + 1] && BoardMass[i, val] == BoardMass[i, val + 2] && BoardMass[i, val] != 0)
					ShowWinner(BoardMass[i, val]); 

				if (BoardMass[val, i] == BoardMass[val + 1, i] && BoardMass[val, i] == BoardMass[val + 2, i] && BoardMass[val, i] != 0)
					ShowWinner(BoardMass[i, val]); 
			}
			if (BoardMass[0, 0] == BoardMass[0 + 1, 0 + 1] && BoardMass[0, 0] == BoardMass[0 + 2, 0 + 2] && BoardMass[0, 0] != 0)
				ShowWinner(BoardMass[0, 0]); 
			if (BoardMass[2, 2] == BoardMass[1, 1] && BoardMass[0, 0] == BoardMass[0, 2] && BoardMass[2, 2] != 0)
				ShowWinner(BoardMass[2, 2]);
		}

		public void ShowWinner(int Player)
		{
			Console.WriteLine($"Игрок {Player} выиграл!");
			IsRunning = false;
		}
	}
	class Program
	{
		static void Main(string[] args)
		{
			Board board = new Board();
			while (board.IsRunning)
			{
				Console.WriteLine("Left - Undo\nRight - Next");
				var key = Console.ReadKey();
				if (key.Key == ConsoleKey.RightArrow)
				{
					int x = 0, y = 0;

					while (!board.Turn(x, y))
					{
						Console.WriteLine("X : ");
						x = Convert.ToInt32(Console.ReadLine());

						Console.WriteLine("Y : ");
						y = Convert.ToInt32(Console.ReadLine());
					}
				}
				else if (key.Key == ConsoleKey.LeftArrow)
				{
					board.Undo();
				}
			}
		}
	}
}
