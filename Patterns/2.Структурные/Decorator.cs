﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator
{
	public abstract class Warrior
	{
		protected string name;
		protected string type;
		protected int attack;
		protected int speed;
		protected int health;
		protected int protection;
		public int GetAttack()
		{
			return attack;
		}
		public int GetSpeed()
		{
			return speed;
		}
		public int GetHealth()
		{
			return health;
		}
		public int GetProtection()
		{
			return protection;
		}
		public void Show()
		{
			Console.WriteLine("");
			Console.WriteLine($"Name : {name}\nType : {type}\nAttac : {attack}\nSpeed : {speed}\nHealth : {health}\nProtection : {protection}");
		}
	}
	class Human : Warrior
	{
		public Human()
		{
			name = "Human";
			type = "-";
			attack = 20;
			speed = 20;
			health = 150;
			protection = 0;
		}
	}
	class Elf : Warrior
	{
		public Elf()
		{
			name = "Elf";
			type = "-";
			attack = 15;
			speed = 30;
			health = 100;
			protection = 0;
		}
	}
	abstract class Decorator : Warrior
	{
		protected Warrior warrior;
		public Decorator(Warrior warrior)
		{
			this.warrior = warrior;
		}
		public void SetComponent(Warrior warrior)
		{
			this.warrior = warrior;
		}
	}
	class HumanWarrior : Decorator
	{
		public HumanWarrior(Warrior warrior) : base(warrior)
		{
			name = "HumanWarrior";
			type = "Human";
			attack = warrior.GetAttack() + 20;
			speed = warrior.GetSpeed() + 20;
			health = warrior.GetHealth() + 10;
			protection = warrior.GetProtection() + 50;
		}
	}
	class Swordsman : HumanWarrior
	{
		public Swordsman(Warrior warrior) : base(warrior)
		{
			name = "Swordsman";
			type = "Human Warrior";
			attack = warrior.GetAttack() + 40;
			speed = warrior.GetSpeed() - 10;
			health = warrior.GetHealth() + 50;
			protection = warrior.GetProtection() + 40;
		}
	}
	class Archer : HumanWarrior
	{
		public Archer(Warrior warrior) : base(warrior)
		{
			name = "Archer";
			type = "Human Warrior";
			attack = warrior.GetAttack() + 20;
			speed = warrior.GetSpeed() + 20;
			health = warrior.GetHealth() + 50;
			protection = warrior.GetProtection() + 10;
		}
	}
	class Rider : Swordsman
	{
		public Rider(Warrior warrior) : base(warrior)
		{
			name = "Rider";
			type = "Swordsman";
			attack = warrior.GetAttack() - 10;
			speed = warrior.GetSpeed() + 40;
			health = warrior.GetHealth() + 200;
			protection = warrior.GetProtection() + 100;
		}
	}
	class ElfWarrior : Decorator
	{
		public ElfWarrior(Warrior warrior) : base(warrior)
		{
			name = "Elf Warrior";
			type = "Elf";
			attack = warrior.GetAttack() + 20;
			speed = warrior.GetSpeed() - 10;
			health = warrior.GetHealth() + 100;
			protection = warrior.GetProtection() + 20;
		}
	}
	class ElfWizzard : Decorator
	{
		public ElfWizzard(Warrior warrior) : base(warrior)
		{
			name = "Elf Wizzard";
			type = "Elf";
			attack = warrior.GetAttack() + 10;
			speed = warrior.GetSpeed() + 10;
			health = warrior.GetHealth() - 50;
			protection = warrior.GetProtection() + 10;
		}
	}
	class Crossbowman : ElfWarrior
	{
		public Crossbowman(Warrior warrior) : base(warrior)
		{
			name = "Crossbowman";
			type = "Elf Warrior";
			attack = warrior.GetAttack() + 20;
			speed = warrior.GetSpeed() + 10;
			health = warrior.GetHealth() + 50;
			protection = warrior.GetProtection() - 10;
		}
	}
	class GoodWizzard : ElfWizzard
	{
		public GoodWizzard(Warrior warrior) : base(warrior)
		{
			name = "Good Wizzard";
			type = "Elf Wizzard";
			attack = warrior.GetAttack() + 50;
			speed = warrior.GetSpeed() + 30;
			health = warrior.GetHealth() + 100;
			protection = warrior.GetProtection() + 30;
		}
	}
	class EvilWizzard : ElfWizzard
	{
		public EvilWizzard(Warrior warrior) : base(warrior)
		{
			name = "Evil Wizzard";
			type = "Elf Wizzard";
			attack = warrior.GetAttack() + 70;
			speed = warrior.GetSpeed() + 20;
			health = warrior.GetHealth() + 0;
			protection = warrior.GetProtection() + 0;
		}
	}
	class Program
	{
		static void Main(string[] args)
		{
			Human human = new Human();
			HumanWarrior humanwarrior = new HumanWarrior(new Human());
			humanwarrior.Show();
			Swordsman swordsman = new Swordsman(new HumanWarrior(new Human()));
			swordsman.Show();
			Elf elf = new Elf();
			elf.Show();
			ElfWizzard elfWizzard = new ElfWizzard(new Elf());
			elfWizzard.Show();
			Warrior evilWizzard = new EvilWizzard(new ElfWizzard(new Elf()));
			evilWizzard.Show();
			Warrior goodWizzard = new GoodWizzard(new ElfWizzard(new Elf()));
			goodWizzard.Show();
		}
	}
}
