﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridge
{
	public interface IPCComponent
	{
		string Name { get; set; }
		void ShowInfo();
	}

	public class CPU : IPCComponent
	{
		public string Name { get; set; } = string.Empty;
		public CPU(string name)
		{
			Name = name;
		}

		public void ShowInfo()
		{
			Console.WriteLine($"CPU: {Name}");
		}
	}

	public class GraphicsCard : IPCComponent
	{
		public string Name { get; set; }
		public GraphicsCard(string name)
		{
			Name = name;
		}

		public void ShowInfo()
		{
			Console.WriteLine($"Graphics Card: {Name}");
		}
	}

	public class SSD : IPCComponent
	{
		public string Name { get; set; }
		public SSD(string name)
		{
			Name = name;
		}
		public void ShowInfo()
		{
			Console.WriteLine($"Graphics Card: {Name}");
		}
	}

	public interface IPCObject
	{
		public abstract void Add(IPCComponent component);
		public abstract void Remove(IPCComponent component);
		public abstract void ShowAllComponents();
	}

	public class PCList
	{
		private List<IPCComponent> components = new List<IPCComponent>();
		public void Add(IPCComponent component)
		{
			components.Add(component);
		}

		public void Remove(IPCComponent component)
		{
			components.Remove(component);
		}
		public List<IPCComponent> GetList()
		{
			return components;
		}
	}

	public class PC : IPCObject
	{
		PCList pclist = new PCList();
		public void Add(IPCComponent component)
		{
			this.pclist.Add(component);
		}

		public void Remove(IPCComponent component)
		{
			this.pclist.Remove(component);
		}

		public void ShowAllComponents()
		{
			foreach (var component in pclist.GetList()) 
			{
				Console.WriteLine($"{component.Name}") ;
			}
		}
	}
	class Program
	{
		static void Main(string[] args)
		{
			PCList pclist = new PCList();
			PC pc = new PC();

			pclist.Add(new GraphicsCard("GTX 1050ti"));
			pclist.Add(new CPU("Intel Core i7 7700"));
			pclist.Add(new SSD("Kingston"));
			foreach (var item in pclist.GetList())
			{
				pc.Add(item);
			}
			pc.ShowAllComponents();
		}
	}
}
