﻿using System;
using System.Collections.Generic;

namespace Composite
{
    abstract class Component
    {
        protected string name;
        protected int price;

        public Component(string name, int price)
        {
            this.name = name;
            this.price = price;
        }

        public virtual void Add(Component component) { }

        public virtual void Remove(Component component) { }

        public virtual void Show() { }
    }

    class OfficeComponent : Component
    {
        private List<Component> components = new List<Component>();

        public OfficeComponent(string name, int price = 0): base(name, price){}

        public override void Add(Component component)
        {
            components.Add(component);
        }

        public override void Remove(Component component)
        {
            components.Remove(component);
        }

        public override void Show()
        {
            Console.WriteLine($"{name}");

            if (components.Count > 0)
            {
                foreach (var item in components)
                {
                    item.Show();
                }
            }
        }
    }

    class Program
	{
		static void Main(string[] args)
		{
            Component reception = new OfficeComponent("Приемная");

            Component receptionStyle = new OfficeComponent("Должна быть выполнена в теплых тонах");
            reception.Add(receptionStyle);

            Component table = new OfficeComponent("Журнальный столик");
            Component magazine = new OfficeComponent("10-20 журналов типа «компьютерный мир»");
            table.Add(magazine);
            reception.Add(table);

            Component sofa = new OfficeComponent("Мягкий диван");
            reception.Add(sofa);

            Component table2 = new OfficeComponent("Стол секретаря");
            Component pc = new OfficeComponent("Компьютер");
            Component hdd = new OfficeComponent("Важно наличие большого объема жесткого диска");
            pc.Add(hdd);
            table2.Add(pc);

            Component tools = new OfficeComponent("Офисный инструментарий");
            table2.Add(tools);
            reception.Add(table2);

            Component water = new OfficeComponent("Кулер с теплой и холодной водой");
            reception.Add(water);

            reception.Show();
        }
	}
}
