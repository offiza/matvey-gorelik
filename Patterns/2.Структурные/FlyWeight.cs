﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlyWeight
{
	enum UnitType
	{
		Infantry,
		Vehicle,
		HeavyVehicle,
		LightVehicle,
		Aerotechnics
	}
	public struct Vector2f
	{
		public Vector2f(int _x, int _y)
		{
			x = _x;
			y = _y;
		}
		public int x { get; set; }
		public int y { get; set; }
	}
	abstract class Unit
	{
		public UnitType type;
		public Vector2f position;
		public string name { get; set; }
		public int speed { get; set; }
		public int power { get; set; }
		public void Show(int x, int y)
		{
			Console.WriteLine($"{name}\nX: {x}\nY: {y}\nPower: {power}\n");
		}
		public void setPosition(int x, int y)
		{
			position.x = x;
			position.y = y;
		}
		public void setPosition(Vector2f pos)
		{
			position = pos;
		}
	}
	class Infantry : Unit
	{
		public Infantry()
		{
			name = "Infantry";
			speed = 10;
			power = 10;
			type =UnitType.Infantry;
		}
	}
	class TransportVehicles : Unit
	{
		public TransportVehicles()
		{
			name = "Transport vehicles";
			speed = 70;
			power = 0;
			type = UnitType.Vehicle;
		}
	}
	class HeavyGroundEquipment : Unit
	{
		public HeavyGroundEquipment()
		{
			name = "Heavy ground equipment";
			speed = 15;
			power = 150;
			type = UnitType.HeavyVehicle;
		}
	}
	class LightgroundEquipment : Unit
	{
		public LightgroundEquipment()
		{
			name = "Light ground equipment";
			speed = 50;
			power = 30;
			type = UnitType.LightVehicle;
		}
	}
	class Aerotechnics : Unit
	{
		public Aerotechnics()
		{
			name = "Aerotechnics";
			speed = 300;
			power = 100;
			type = UnitType.Aerotechnics;
		}
	}
	class UnitFactory
	{
		private Dictionary<UnitType, Unit> existingUnitTypes = new Dictionary<UnitType, Unit>();
		public Unit setUnit(UnitType type, int x, int y)
		{
			Unit unit = null;
			if (existingUnitTypes.ContainsKey(type))
			{
				unit = existingUnitTypes[type];
				unit.Show(x, y);
			}
			else
			{
				switch (type)
				{
					case UnitType.Aerotechnics:
						unit = new Aerotechnics();
						break;
					case UnitType.HeavyVehicle:
						unit = new HeavyGroundEquipment();
						break;
					case UnitType.Infantry:
						unit = new Infantry();
						break;
					case UnitType.LightVehicle:
						unit = new LightgroundEquipment();
						break;
					case UnitType.Vehicle:
						unit = new TransportVehicles();
						break;
				}
				existingUnitTypes.Add(type, unit);
				unit.Show(x, y);
			}
			return unit;
		}
	}
	class Program
	{
		static void Main(string[] args)
		{
			UnitFactory factory = new UnitFactory();
			factory.setUnit(UnitType.Aerotechnics, 100, 200);
			factory.setUnit(UnitType.Aerotechnics, 50, 250);
			factory.setUnit(UnitType.HeavyVehicle, 375, 23);
			factory.setUnit(UnitType.HeavyVehicle, 345, 123);
		}
	}
}
