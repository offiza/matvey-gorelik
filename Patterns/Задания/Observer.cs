﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Паттерн "Наблюдатель". Проект "Оповещение постов ГАИ". В проекте должна быть реализована отправка сообщений всем постам ГАИ.*/
namespace Observer
{
	public interface IGAIDepartment
	{ 
		void Update(Notifier policeNotifier, string message);
	}
	public class Notifier
	{
		private List<IGAIDepartment> departments;
		public Notifier()
		{
			departments = new List<IGAIDepartment>();
		}
		public void AddDepartment(IGAIDepartment department)
		{
			departments.Add(department);
		}
		public void RemoveDepartment(IGAIDepartment department)
		{
			departments.Remove(department);
		}
		public void ClearDepartment()
		{
			departments.Clear();
		}
		public void Notify(string message)
		{
			foreach (var d in departments)
			{
				d.Update(this, message);
			}
		}
	}
	public class GAIDepartment : IGAIDepartment
	{
		string Name { get; set; }
		public GAIDepartment(string name)
		{
			Name = name;
		}
		void IGAIDepartment.Update(Notifier gaiNotifier, string message)
		{
			Console.WriteLine($"Оповещаем {Name} про {message}");
		}
	}
	class Program
	{
		static void Main(string[] args)
		{
			GAIDepartment department1 = new GAIDepartment("ГАИ Одесса");
			GAIDepartment department2 = new GAIDepartment("ГАИ Киев");
			GAIDepartment department3 = new GAIDepartment("ГАИ Львов");
			Notifier notifier = new Notifier();

			notifier.AddDepartment(department1);
			notifier.AddDepartment(department2);
			notifier.AddDepartment(department3);
			notifier.Notify("Учебная проверка");
			notifier.ClearDepartment();
		}
	}
}