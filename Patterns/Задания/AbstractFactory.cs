﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Паттерн "Абстрактная фабрика". Проект "Заводы по производству автомобилей". В проекте должно быть реализована возможность создавать
автомобили различных типов на разных заводах.*/
namespace AbastractFactory
{
	public interface IAbstractFactory
	{
		ISedan CreateSedan();

		IHatchback CreateHatchBack();
	}
	public interface ISedan
	{
		void Show();
	}
	public interface IHatchback
	{
		void Show();
	}
	class BMWSedan : ISedan
	{
		public void Show()
		{
			Console.WriteLine("BMW sedan");
		}
	}
	class BMWHatchback : IHatchback
	{
		public void Show()
		{
			Console.WriteLine("BMW hatchback");
		}
	}
	class MercedesSedan : ISedan
	{
		public void Show()
		{
			Console.WriteLine("Mercedes sedan");
		}
	}
	class MercedesHatchback : IHatchback
	{
		public void Show()
		{
			Console.WriteLine("Mercedes hatchback");
		}
	}
	class BMWFactory : IAbstractFactory
	{
		public IHatchback CreateHatchBack()
		{
			return new BMWHatchback();
		}

		public ISedan CreateSedan()
		{
			return new BMWSedan();
		}
	}
	class MercedesFactory : IAbstractFactory
	{
		public IHatchback CreateHatchBack()
		{
			return new MercedesHatchback();
		}

		public ISedan CreateSedan()
		{
			return new MercedesSedan();
		}
	}
	class Program
	{
		static void Main(string[] args)
		{
			IAbstractFactory bmw = new BMWFactory();
			var product = bmw.CreateHatchBack();
			product.Show();		
		}
	}
}
