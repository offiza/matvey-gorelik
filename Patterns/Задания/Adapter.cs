﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Паттерн "Адаптер". Проект "Часы". В проекте должен быть реализован адаптер, который дает возможность пользоваться часами со стрелками так же,
как и цифровыми часами. В классе "Часы со стрелками" хранятся повороты стрелок.*/
namespace Adapter
{
	class Clock
	{
		private int hours;
		private int minutes;
		private bool tod;
		public Clock(int h, int m, bool t)
		{
			if (h >= 0 && h <= 11 && m >= 0 && m <= 59)
			{
				hours = h;
				minutes = m;
				tod = t;
			}
			else
			{
				hours = 0;
				minutes = 0;
				tod = true;
			}
		}
		void ShowTime()
		{
			Console.WriteLine($"Clock -  {hours} : {minutes}");
		}
		public int GetHours()
		{
			return hours;
		}
		public int GetMinutes()
		{
			return minutes;
		}
		public bool GetTod()
		{
			return tod;
		}
	}
	class DigitalClock
	{
		public virtual void ShowTime() { }
	};
	class AdapterClock : DigitalClock
	{
		private Clock clock;
		public AdapterClock(Clock c)
		{
			clock = c;
		}
		public override void ShowTime()
		{
			Console.WriteLine("Digital Clocks");

			int a_hours = clock.GetHours();
			int a_minutes = clock.GetMinutes();

			if (clock.GetTod() == false)
			{
				Console.WriteLine($"{a_hours + 12} : {a_minutes}");
			}
			else
			{
				Console.WriteLine($"{a_hours} : {a_minutes}");
			}
		}
	}
	class Program
	{
		static void Main(string[] args)
		{
			DigitalClock digital = new AdapterClock(new Clock(11, 50, false));
			digital.ShowTime();
		}
	}
}
