﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMethod
{
	abstract class Smartphone
	{
		protected string name;
		protected string cpu;
		protected string battery;
		protected string system;
		public static Smartphone CreateSmartphone(int type)
		{
			if (type == 1)
				return new Iphone();
			else if (type == 2)
				return new Samsung();
			else if (type == 3)
				return new Lenovo();
			return null;
		}
		public void Show()
		{
			Console.WriteLine($"Name : {name}\nCPU : {cpu}\nBattery : {battery}\nsystem : {system}");
		}
	}
	class Iphone : Smartphone
	{
		public Iphone()
		{
			name = "Iphone";
			cpu = "A12 bionic";
			battery = "3000mAh";
			system = "ios";
		}
	}
	class Samsung : Smartphone
	{
		public Samsung()
		{
			name = "Samsung";
			cpu = "Snapdragon 855";
			battery = "3200mAh";
			system = "android 9";
		}
	}
	class Lenovo : Smartphone
	{
		public Lenovo()
		{
			name = "Lenovo";
			cpu = "Snapdragon 730";
			battery = "5000mAh";
			system = "android 9";
		}
	}
	class Program
	{
		static void Main(string[] args)
		{
			List<Smartphone> smartphones = new List<Smartphone>
			{
				Smartphone.CreateSmartphone(1)
			};
			for (int i = 0; i < smartphones.Count; i++)
			{
				smartphones.ElementAt(i).Show();
			}
		}
	}
}
