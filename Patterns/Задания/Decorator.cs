﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Паттерн "Декоратор". Проект "Универсальная электронная карта". В проекте должна быть реализована универсальная электронная карта, 
в которой есть функции паспорта, страхового полиса, банковской карты и т. д.*/
namespace Decorator
{
	public abstract class Card
	{
		public abstract void Operation();
	}
	class ElectronicCard : Card
	{
		public override void Operation()
		{
			Console.WriteLine("Electronic Card");
		}
	}
	abstract class UltraCard : Card
	{
		protected Card card;
		public UltraCard(Card card)
		{
			this.card = card;
		}
		public void SetComponent(Card card)
		{
			this.card = card;
		}
		public void Remove()
		{
			card = null;
		}
		public override void Operation()
		{
			if (card != null)
			{
				Console.WriteLine("Ultra Card");
			}
			else
			{
				Console.Write("");
			}
		}
	}
	class Passport : UltraCard
	{
		public Passport(Card card) : base(card)
		{
		}
		public override void Operation()
		{
			Console.WriteLine("Passport");
		}
	}
	class BankCard : UltraCard
	{
		public BankCard(Card card) : base(card)
		{
		}
		public override void Operation()
		{
			Console.WriteLine("Bank Card");
		}
	}
	class InsurancePolicy : UltraCard
	{
		public InsurancePolicy(Card card) : base(card)
		{
		}
		public override void Operation()
		{
			Console.WriteLine("Insurance policy");
		}
	}
	public class Client
	{
		public void UI(Card card)
		{
			card.Operation();
		}
	}
	class Program
	{
		static void Main(string[] args)
		{
			Client client = new Client();
			ElectronicCard card = new ElectronicCard();
			client.UI(card);
			Passport passport = new Passport(card);
			InsurancePolicy insurancePolicy = new InsurancePolicy(card);
			client.UI(passport);
			client.UI(insurancePolicy);
		}
	}
}
