﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Паттерн "Стратегия". Проект "Принтеры". В проекте должны быть реализованы разные модели принтеров, которые выполняют разные виды печати.*/
namespace Strategy
{
	interface IPrint
	{
		void Print();
	}
	class LaserPrint : IPrint
	{
		public void Print()
		{
			Console.WriteLine("Laser Print");
		}
	}
	class JetPrinter : IPrint
	{
		public void Print()
		{
			Console.WriteLine("Jet Print");
		}
	}
	class Printer
	{
		IPrint print;
		public Printer(IPrint p)
		{
			print = p;
		}
		public void Show()
		{
			print.Print();
		}
	}
	class Program
	{
		static void Main(string[] args)
		{
			Printer printer = new Printer(new JetPrinter());
			printer.Show();
		}
	}
}
