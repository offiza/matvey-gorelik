﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*Паттерн "Фасад". Проект "Компьютер". В проекте должен быть реализован "компьютер", который выполняет основные функции, к примеру, включение, выключение, */
namespace Facade
{
	public class GraphicsCard
	{
		public void Start()
		{
			Console.WriteLine("Graphics Card : Start");
		}
		public void CheckConection()
		{
			Console.WriteLine("Graphics Card : Check conection with monitor");
		}
		public void ShowRAM()
		{
			Console.WriteLine("Graphics Card : Show RAM");
		}
		public void ShowDiscReader()
		{
			Console.WriteLine("Graphics Card : Show disc reader");
		}
		public void ShowHardDisc()
		{
			Console.WriteLine("Graphics Card : Show hard disc");

		}
		public void End()
		{
			Console.WriteLine("Graphics Card : End");
		}
	}
	public class RAM
	{
		public void LaunchDevices()
		{
			Console.WriteLine("RAM : launch devices");
		}
		public void MemoryAnalysis()
		{
			Console.WriteLine("RAM : launch devices");
		}
		public void Clear()
		{
			Console.WriteLine("RAM : clear");
		}
	}
	public class HardDisc
	{
		public void Start()
		{
			Console.WriteLine("Hard disc : Start");
		}
		public void BootCheck()
		{
			Console.WriteLine("Hard disc : Boot sector check");
		}
		public void End()
		{
			Console.WriteLine("HardDisc Card : End");
		}
	}
	public class OpticalDiscReader
	{
		public void Start()
		{
			Console.WriteLine("Optical disc reader : Start");
		}
		public void DiscCheck()
		{
			Console.WriteLine("Optical disc reader : Disc check");
		}
		public void End()
		{
			Console.WriteLine("OpticalDiscReader Card : End");
		}
	}
	public class PowerSupply
	{
		public void ApplyPower()
		{
			Console.WriteLine("Power Supply : Apply power");
		}
		public void ApplyPowerGC()
		{
			Console.WriteLine("Power Supply : Apply power to graphics card");
		}
		public void ApplyPowerRAM()
		{
			Console.WriteLine("Power Supply : Apply power to RAM");
		}
		public void ApplyPowerODR()
		{
			Console.WriteLine("Power Supply : Apply power to optical disc reader");
		}
		public void ApplyPowerHD()
		{
			Console.WriteLine("Power Supply : Apply power to hard disc");
		}
	}
	public class Sensors
	{
		public void CheckVoltage()
		{
			Console.WriteLine("Sensors : check voltage");
		}
		public void CheckTemperaturePS()
		{
			Console.WriteLine("Sensors : check temperature in power supply");
		}
		public void CheckTemperatureGC()
		{
			Console.WriteLine("Sensors : check temperature in graphics card");
		}
		public void CheckTemperatureRAM()
		{
			Console.WriteLine("Sensors : check temperature in RAM");
		}
		public void ChecTemperature()
		{
			Console.WriteLine("Sensors : check temperature ");
		}
	}

	public class Computer
	{
		protected GraphicsCard graphicsCard = new GraphicsCard();
		protected RAM ram = new RAM();
		protected HardDisc hardDisc = new HardDisc();
		protected OpticalDiscReader opticalDiscReader = new OpticalDiscReader();
		protected PowerSupply powerSupply = new PowerSupply();
		protected Sensors sensors = new Sensors();
		public void BeginWork()
		{
			powerSupply.ApplyPower();
			sensors.CheckVoltage();
			sensors.CheckTemperaturePS();
			sensors.CheckTemperatureGC();
			powerSupply.ApplyPowerGC();
			graphicsCard.Start();
			graphicsCard.CheckConection();
			sensors.CheckTemperatureRAM();
			powerSupply.ApplyPowerRAM();
			ram.LaunchDevices();
			ram.MemoryAnalysis();
			graphicsCard.ShowRAM();
			powerSupply.ApplyPowerHD();
			opticalDiscReader.Start();
			opticalDiscReader.DiscCheck();
			graphicsCard.ShowDiscReader();
			powerSupply.ApplyPowerHD();
			hardDisc.Start();
			hardDisc.BootCheck();
			graphicsCard.ShowHardDisc();
			sensors.ChecTemperature();
		}
		public void EndWork()
		{
			ram.Clear();
			graphicsCard.End();
			opticalDiscReader.End();
			hardDisc.End();
		}
		public void CheckComputer()
		{
			Console.WriteLine("");
			sensors.CheckTemperatureGC();
			sensors.CheckTemperaturePS();
			sensors.CheckTemperatureRAM();
			sensors.CheckVoltage();
			sensors.ChecTemperature();
			Console.WriteLine("Computer status is normal");
		}
	}
	class Program
	{
		static void Main(string[] args)
		{
			Computer computer = new Computer();
			computer.BeginWork();
			computer.CheckComputer();
			computer.EndWork();
		}
	}
}
