﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tetris
{
	class Program
	{
		abstract class Figure
		{
			protected int[,] points;
			protected string name;
			public virtual void SetFigure()
			{

			}
			public virtual void ShowFigure()
			{
				Console.WriteLine("Abstract Figure");
			}
			public static Figure CreateFig(int type)
			{

				if (type == 1)
					return new FirstFigure();
				else if (type == 2)
					return new SecondFigure();
				else if (type == 3)
					return new ThirdFigure();
				return null;
			}
		}
		class FirstFigure : Figure
		{
			public FirstFigure()
			{
				points = new int[4, 4];
				name = "Figure1";
				SetFigure();
			}
			public override void SetFigure()
			{
				Random random = new Random();
				for (int i = 0; i < 4; i++)
				{
					for (int j = 0; j < 4; j++)
					{
						points[i, j] = random.Next(0,2);
					}
				}
			}
			public override void ShowFigure()
			{
				Console.WriteLine($"name: {name}");
				for (int i = 0; i < 4; i++)
				{
					for (int j = 0; j < 4; j++)
					{
						if(points[i, j] == 1) 
							Console.Write("*");
					}
					Console.WriteLine();
				}
			}
		}

		class SecondFigure : Figure
		{
			public SecondFigure()
			{
				points = new int[4, 4];
				name = "Figure2";
				SetFigure();
			}
			public override void SetFigure()
			{
				Random random = new Random();
				for (int i = 0; i < 4; i++)
				{
					for (int j = 0; j < 4; j++)
					{
						points[i, j] = random.Next(0, 3);
					}
				}
			}
			public override void ShowFigure()
			{
				Console.WriteLine($"name: {name}");
				for (int i = 0; i < 4; i++)
				{
					for (int j = 0; j < 4; j++)
					{
						if (points[i, j] == 2)
							Console.Write("*");
					}
					Console.WriteLine();
				}
			}
		}

		class ThirdFigure : Figure
		{
			public ThirdFigure()
			{
				points = new int[4, 4];
				name = "Figure3";
				SetFigure();
			}
			public override void SetFigure()
			{
				Random random = new Random();
				for (int i = 0; i < 4; i++)
				{
					for (int j = 0; j < 4; j++)
					{
						points[i, j] = random.Next(1, 4);
					}
				}
			}
			public override void ShowFigure()
			{
				Console.WriteLine($"name: {name}");
				for (int i = 0; i < 4; i++)
				{
					for (int j = 0; j < 4; j++)
					{
						if (points[i, j] == 3)
							Console.Write("*");
					}
					Console.WriteLine();
				}
			}
		}

		static void Main(string[] args)
		{
			List<Figure> figures = new List<Figure>();
			figures.Add(Figure.CreateFig(3)); 
			for (int i = 0; i < figures.Count; i++)
			{
				figures.ElementAt(i).ShowFigure();
			}
		}
	}
}
