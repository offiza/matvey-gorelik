﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace builder
{
	class Auto
	{
		private string name;
		private string body;
		private string engine;
		private string wheels;
		private string gearbox;
		public void SetName(string name)
		{
			this.name = name;
		}
		public void SetBody(string body)
		{
			this.body = body;
		}
		public void SetEngine(string engine)
		{
			this.engine = engine;
		}
		public void SetWheels(string wheels)
		{
			this.wheels = wheels;
		}
		public void SetGearbox(string gearbox)
		{
			this.gearbox = gearbox;
		}
		public void ShowAuto()
		{
			Console.WriteLine($"Name : {name}\nBody : {body}\nEngine : {engine}\nWheels : {wheels}\nGearbox : {gearbox}");
			Console.WriteLine();
		}
	}
	public interface IBuilderAuto
	{
		void Name();
		void Body();
		void Engine();
		void Wheels();
		void GearBox();
	}

	class DaewooNexiaBuilder : IBuilderAuto
	{
		private Auto auto = new Auto();
		public DaewooNexiaBuilder()
		{
			this.Reset();
		}
		public void Reset()
		{
			this.auto = new Auto();
		} 
		public void Body()
		{
			auto.SetBody("sedan");
		}
		public void Engine()
		{
			auto.SetEngine("98 horsepower");
		}
		public void GearBox()
		{
			auto.SetGearbox("5 Manual");
		}
		public void Name()
		{
			auto.SetName("Daewoo Nexia");
		}
		public void Wheels()
		{
			auto.SetWheels("14");
		}
		public void GetProduct()
		{
			auto.ShowAuto();
		}
	}
	class UAZPatrickBuilder : IBuilderAuto
	{
		private Auto auto = new Auto();
		public UAZPatrickBuilder()
		{
			this.Reset();
		}
		public void Reset()
		{
			this.auto = new Auto();
		}
		public void Body()
		{
			auto.SetBody("Jeep");
		}
		public void Engine()
		{
			auto.SetEngine("120 horsepower");
		}
		public void GearBox()
		{
			auto.SetGearbox("4 Manual");
		}
		public void Name()
		{
			auto.SetName("UAZ Patrick");
		}
		public void Wheels()
		{
			auto.SetWheels("16");
		}
		public void GetProduct()
		{
			auto.ShowAuto();
		}
	}
	class OkaziaBuilder : IBuilderAuto
	{
		private Auto auto = new Auto();
		public OkaziaBuilder()
		{
			this.Reset();
		}
		public void Reset()
		{
			this.auto = new Auto();
		}
		public void Body()
		{
			auto.SetBody("HetchbAck");
		}
		public void Engine()
		{
			auto.SetEngine("39 horsepower");
		}
		public void GearBox()
		{
			auto.SetGearbox("3 Manual");
		}
		public void Name()
		{
			auto.SetName("OKAzia");
		}
		public void Wheels()
		{
			auto.SetWheels("11");
		}
		public void GetProduct()
		{
			auto.ShowAuto();
		}
	}
	public class Shop
	{
		public void buildNexia()
		{
			DaewooNexiaBuilder Nexia = new DaewooNexiaBuilder();
			Nexia.GearBox();
			Nexia.Name();
			Nexia.Wheels();
			Nexia.Engine();
			Nexia.Body();
			Nexia.GetProduct();
		}
		public void buildPatrick()
		{
			UAZPatrickBuilder UAZ = new UAZPatrickBuilder();
			UAZ.GearBox();
			UAZ.Name();
			UAZ.Wheels();
			UAZ.Engine();
			UAZ.Body();
			UAZ.GetProduct();
		}
	}
	class Program
	{
		static void Main(string[] args)
		{
			Shop shop = new Shop();
			shop.buildNexia();
			shop.buildPatrick();
		}
	}
}
