﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactory
{
	public interface IContinent
	{
		Herbivore CreateHerbivore();
		Carnivore CreateCarnivore();
	}
	public abstract class Herbivore
	{
		protected string name;
		protected int weight;
		protected bool life;
		public abstract void EatGrass();
		public int GetWeight()
		{
			return weight;
		}
		public void Death()
		{
			life = false;
			Console.WriteLine($"{name} was killed");
		}
	}
	public abstract class Carnivore
	{
		protected string name;
		protected int power;
		public abstract void Eat(Herbivore herbivore);
	}

	class Afrika : IContinent
	{
		private Afrika() { }
		private static Afrika afrika;

		public static Afrika GetAfrika()
		{
			if (afrika == null)
			{
				afrika = new Afrika();
			}
			return afrika;
		}
		public Carnivore CreateCarnivore()
		{
			return new Lion();
		}
		public Herbivore CreateHerbivore()
		{
			return new Wildebeest();
		}
	}
	class NorthAmerika : IContinent
	{
		private NorthAmerika() { }
		private static NorthAmerika northAmerika;

		public static NorthAmerika GetAmerika()
		{
			if (northAmerika == null)
			{
				northAmerika = new NorthAmerika();
			}
			return northAmerika;
		}
		public Carnivore CreateCarnivore()
		{
			return new Wolf();
		}
		public Herbivore CreateHerbivore()
		{
			return new Bison();
		}
	}
	class Eurasia : IContinent
	{
		private Eurasia() { }
		private static Eurasia eurasia;

		public static Eurasia GetEurasia()
		{
			if (eurasia == null)
			{
				eurasia = new Eurasia();
			}
			return eurasia;
		}
		public Carnivore CreateCarnivore()
		{
			return new Tiger();
		}
		public Herbivore CreateHerbivore()
		{
			return new Elk();
		}
	}

	class Wildebeest : Herbivore
	{
		public Wildebeest()
		{
			name = "Wildebeest";
			weight = 50;
		}
		public override void EatGrass()
		{
			this.weight += 10;
			Console.WriteLine($"weight of {name} : {this.weight}");
		}
	}
	class Bison : Herbivore
	{
		public Bison()
		{
			name = "Bison";
			weight = 100;
		}
		public override void EatGrass()
		{
			this.weight += 11;
			Console.WriteLine($"weight of {name} : {this.weight}");
		}
	}
	class Elk : Herbivore
	{
		public Elk()
		{
			name = "Elk";
			weight = 70;
		}

		public override void EatGrass()
		{
			this.weight += 12;
			Console.WriteLine($"weight of {name} : {this.weight}");
		}
	}

	class Lion : Carnivore
	{
		public Lion()
		{
			name = "Lion";
			power = 50;
		}
		public override void Eat(Herbivore herbivore)
		{
			if (this.power >= herbivore.GetWeight())
			{
				power += 10;
				Console.WriteLine($"power of {name} : {power}");
				herbivore.Death();
			}
			else
			{
				power -= 10;
				Console.WriteLine($"power of {name} : {power}");
			}
		}
	}
	class Wolf : Carnivore
	{
		public Wolf()
		{
			name = "Wolf";
			power = 30;
		}
		public override void Eat(Herbivore herbivore)
		{
			if (this.power >= herbivore.GetWeight())
			{
				power += 5;
				Console.WriteLine($"power of {name} : {power}");
				herbivore.Death();
			}
			else
			{
				power -= 5;
				Console.WriteLine($"power of {name} : {power}");
			}
		}
	}
	class Tiger : Carnivore
	{
		public Tiger()
		{
			name = "Tiger";
			power = 60;
		}
		public override void Eat(Herbivore herbivore)
		{
			if (this.power >= herbivore.GetWeight())
			{
				power += 7;
				Console.WriteLine($"power of {name} : {power}");
				herbivore.Death();
			}
			else
			{
				power -= 7;
				Console.WriteLine($"power of {name} : {power}");
			}
		}
	}

	class AnimalWorld
	{
		public void MealsHerbivores(IContinent continent)
		{
			var herbivore = continent.CreateHerbivore();
			herbivore.EatGrass();
		}
		public void NutritionCarnivores(IContinent continent)
		{
			var carnivore = continent.CreateCarnivore();
			var herbivore = continent.CreateHerbivore();
			carnivore.Eat(herbivore);
		}
	}

	class Program
	{
		static void Main(string[] args)
		{
			AnimalWorld animalWorld = new AnimalWorld();
			Afrika afrika = Afrika.GetAfrika();
			Eurasia eurasia = Eurasia.GetEurasia();
			NorthAmerika northAmerika = NorthAmerika.GetAmerika();

			animalWorld.MealsHerbivores(afrika);
			animalWorld.NutritionCarnivores(afrika);
		}
	}
}
