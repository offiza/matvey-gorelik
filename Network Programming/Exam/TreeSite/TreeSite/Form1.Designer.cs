﻿namespace TreeSite
{
	partial class Form1
	{
		/// <summary>
		/// Обязательная переменная конструктора.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Освободить все используемые ресурсы.
		/// </summary>
		/// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Код, автоматически созданный конструктором форм Windows

		/// <summary>
		/// Требуемый метод для поддержки конструктора — не изменяйте 
		/// содержимое этого метода с помощью редактора кода.
		/// </summary>
		private void InitializeComponent()
		{
			this.buttonStart = new System.Windows.Forms.Button();
			this.textBoxPath = new System.Windows.Forms.TextBox();
			this.richTextBoxUI = new System.Windows.Forms.RichTextBox();
			this.SuspendLayout();
			// 
			// buttonStart
			// 
			this.buttonStart.Location = new System.Drawing.Point(234, 11);
			this.buttonStart.Name = "buttonStart";
			this.buttonStart.Size = new System.Drawing.Size(75, 23);
			this.buttonStart.TabIndex = 0;
			this.buttonStart.Text = "Start";
			this.buttonStart.UseVisualStyleBackColor = true;
			this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
			// 
			// textBoxPath
			// 
			this.textBoxPath.BackColor = System.Drawing.SystemColors.Menu;
			this.textBoxPath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.textBoxPath.Location = new System.Drawing.Point(12, 12);
			this.textBoxPath.Name = "textBoxPath";
			this.textBoxPath.Size = new System.Drawing.Size(216, 22);
			this.textBoxPath.TabIndex = 1;
			this.textBoxPath.Text = "http://google.com";
			// 
			// richTextBoxUI
			// 
			this.richTextBoxUI.BackColor = System.Drawing.SystemColors.Menu;
			this.richTextBoxUI.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.richTextBoxUI.Location = new System.Drawing.Point(12, 40);
			this.richTextBoxUI.Name = "richTextBoxUI";
			this.richTextBoxUI.Size = new System.Drawing.Size(297, 415);
			this.richTextBoxUI.TabIndex = 2;
			this.richTextBoxUI.Text = "";
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.White;
			this.ClientSize = new System.Drawing.Size(322, 464);
			this.Controls.Add(this.richTextBoxUI);
			this.Controls.Add(this.textBoxPath);
			this.Controls.Add(this.buttonStart);
			this.Name = "Form1";
			this.Text = "Form1";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button buttonStart;
		private System.Windows.Forms.TextBox textBoxPath;
		private System.Windows.Forms.RichTextBox richTextBoxUI;
	}
}

