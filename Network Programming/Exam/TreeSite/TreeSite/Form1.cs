﻿using System;
using System.Collections.Generic;
using HtmlAgilityPack;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace TreeSite
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private void buttonStart_Click(object sender, EventArgs e)
		{
			if (textBoxPath.Text != "")
			{
				Task parse = new Task(() =>
				{
					Parse(textBoxPath.Text);
				});
				parse.Start();
			}
			else
				MessageBox.Show("Введите путь!");
		}

		private List<string> Alllinks(string url)
		{
			List<string> links = new List<string>();

			HtmlAgilityPack.HtmlDocument document = new HtmlWeb().Load(url);

			ParseTag(document, links, "a", "href");

			return links;
		}

		private void ParseTag(HtmlAgilityPack.HtmlDocument doc, List<string> links, string tag, string param)
		{

			HtmlNodeCollection tags = doc.DocumentNode.SelectNodes($"//{tag}");

			if (tags != null)
			{
				foreach (HtmlNode t in tags)
				{
					string value = t.GetAttributeValue(param, "");

					if (value.Length == 0 || value.IndexOf('#') != -1)
					{
						return;
					}

					if (links.Where(l => l.Equals(value)).ToArray().Length == 0)
					{
						links.Add(value);
					}
				}
			}
		}


		private void Parse(string siteUrl, string url = "", string level = "")
		{
			if (url.Length == 0)
				url = siteUrl;
			Regex regex = new Regex($"{url}");
			var links = Alllinks(url);

			richTextBoxUI.Text += $"{level}{url}\n";

			for (int i = 0; i < links.Count; i++)
			{
				links[i] = links[i];

				richTextBoxUI.Text += $"{level}{links[i]}\n";

				if (!regex.Match(links[i]).Success)
				{
					continue;
				}
			}
		}
	}
}
