﻿using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace UdpChat
{
    class Program
    {
		// Локальный порт для прослушивания
		private static int localPort = 5000;
		// Удаленный IP и порт для подключения
		private static IPAddress remoteIPAddr = IPAddress.Parse("127.0.0.1"); // для локальных операций на одной машине

		static void ReceiveThreadProc()
        {
            try
            {
                while (true)
                {
					// Создаем UdpClient для чтения входящих данных
					var udpClient = new UdpClient(localPort);

                    IPEndPoint ipEnd = null;
                    byte[] responce = udpClient.Receive(ref ipEnd);

                    string strResult = Encoding.Unicode.GetString(responce);
                    Console.WriteLine(strResult);

                    udpClient.Close();
                }
            }
            catch (SocketException sockEx)
            {
                Console.WriteLine($"Ошибка: {sockEx.Message}");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Ошибка: {ex.Message}");
            }
        }
		static void Main(string[] args)
        {
            Console.Title = "Client";

            try
            {
                ReceiveThreadProc();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Ошибка: {ex.Message}");
            }
        }
    }
}
