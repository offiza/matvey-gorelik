﻿namespace CSC.Client
{
	partial class LoginForm
	{
		/// <summary>
		/// Обязательная переменная конструктора.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Освободить все используемые ресурсы.
		/// </summary>
		/// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Код, автоматически созданный конструктором форм Windows

		/// <summary>
		/// Требуемый метод для поддержки конструктора — не изменяйте 
		/// содержимое этого метода с помощью редактора кода.
		/// </summary>
		private void InitializeComponent()
		{
			this.buttonLogin = new System.Windows.Forms.Button();
			this.labelName = new System.Windows.Forms.Label();
			this.labelIP = new System.Windows.Forms.Label();
			this.textBoxName = new System.Windows.Forms.TextBox();
			this.textBoxIP = new System.Windows.Forms.TextBox();
			this.buttonExit = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// buttonLogin
			// 
			this.buttonLogin.Location = new System.Drawing.Point(95, 154);
			this.buttonLogin.Margin = new System.Windows.Forms.Padding(2);
			this.buttonLogin.Name = "buttonLogin";
			this.buttonLogin.Size = new System.Drawing.Size(170, 31);
			this.buttonLogin.TabIndex = 0;
			this.buttonLogin.Text = "Login";
			this.buttonLogin.UseVisualStyleBackColor = true;
			this.buttonLogin.Click += new System.EventHandler(this.buttonLogin_Click);
			// 
			// labelName
			// 
			this.labelName.AutoSize = true;
			this.labelName.Location = new System.Drawing.Point(8, 6);
			this.labelName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.labelName.Name = "labelName";
			this.labelName.Size = new System.Drawing.Size(49, 17);
			this.labelName.TabIndex = 1;
			this.labelName.Text = "Name:";
			// 
			// labelIP
			// 
			this.labelIP.AutoSize = true;
			this.labelIP.Location = new System.Drawing.Point(11, 86);
			this.labelIP.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.labelIP.Name = "labelIP";
			this.labelIP.Size = new System.Drawing.Size(24, 17);
			this.labelIP.TabIndex = 2;
			this.labelIP.Text = "IP:";
			// 
			// textBoxName
			// 
			this.textBoxName.Location = new System.Drawing.Point(11, 25);
			this.textBoxName.Margin = new System.Windows.Forms.Padding(2);
			this.textBoxName.Name = "textBoxName";
			this.textBoxName.Size = new System.Drawing.Size(351, 22);
			this.textBoxName.TabIndex = 3;
			this.textBoxName.Text = "User";
			this.textBoxName.TextChanged += new System.EventHandler(this.textBoxes_TextChanged);
			// 
			// textBoxIP
			// 
			this.textBoxIP.Location = new System.Drawing.Point(11, 105);
			this.textBoxIP.Margin = new System.Windows.Forms.Padding(2);
			this.textBoxIP.Name = "textBoxIP";
			this.textBoxIP.Size = new System.Drawing.Size(351, 22);
			this.textBoxIP.TabIndex = 4;
			this.textBoxIP.Text = "127.0.0.1";
			this.textBoxIP.TextChanged += new System.EventHandler(this.textBoxes_TextChanged);
			// 
			// buttonExit
			// 
			this.buttonExit.Location = new System.Drawing.Point(95, 221);
			this.buttonExit.Margin = new System.Windows.Forms.Padding(2);
			this.buttonExit.Name = "buttonExit";
			this.buttonExit.Size = new System.Drawing.Size(170, 31);
			this.buttonExit.TabIndex = 5;
			this.buttonExit.Text = "Exit";
			this.buttonExit.UseVisualStyleBackColor = true;
			this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
			// 
			// LoginForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(373, 272);
			this.Controls.Add(this.buttonExit);
			this.Controls.Add(this.textBoxIP);
			this.Controls.Add(this.textBoxName);
			this.Controls.Add(this.labelIP);
			this.Controls.Add(this.labelName);
			this.Controls.Add(this.buttonLogin);
			this.Margin = new System.Windows.Forms.Padding(2);
			this.Name = "LoginForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Login Form";
			this.Load += new System.EventHandler(this.LoginForm_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button buttonLogin;
		private System.Windows.Forms.Label labelName;
		private System.Windows.Forms.Label labelIP;
		private System.Windows.Forms.TextBox textBoxName;
		private System.Windows.Forms.TextBox textBoxIP;
		private System.Windows.Forms.Button buttonExit;
	}
}

