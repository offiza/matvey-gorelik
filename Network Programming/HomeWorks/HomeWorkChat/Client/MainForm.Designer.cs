﻿namespace Client
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.textBoxChat = new System.Windows.Forms.TextBox();
			this.textBoxMessage = new System.Windows.Forms.TextBox();
			this.buttonSend = new System.Windows.Forms.Button();
			this.listBoxUsers = new System.Windows.Forms.ListBox();
			this.labelUsersList = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// textBoxChat
			// 
			this.textBoxChat.Location = new System.Drawing.Point(217, 11);
			this.textBoxChat.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.textBoxChat.Multiline = true;
			this.textBoxChat.Name = "textBoxChat";
			this.textBoxChat.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.textBoxChat.Size = new System.Drawing.Size(468, 344);
			this.textBoxChat.TabIndex = 0;
			// 
			// textBoxMessage
			// 
			this.textBoxMessage.Location = new System.Drawing.Point(217, 359);
			this.textBoxMessage.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.textBoxMessage.Name = "textBoxMessage";
			this.textBoxMessage.Size = new System.Drawing.Size(468, 22);
			this.textBoxMessage.TabIndex = 1;
			this.textBoxMessage.TextChanged += new System.EventHandler(this.textBoxMessage_TextChanged);
			this.textBoxMessage.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxMessage_KeyDown);
			// 
			// buttonSend
			// 
			this.buttonSend.Location = new System.Drawing.Point(582, 389);
			this.buttonSend.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.buttonSend.Name = "buttonSend";
			this.buttonSend.Size = new System.Drawing.Size(103, 26);
			this.buttonSend.TabIndex = 2;
			this.buttonSend.Text = "Отправить";
			this.buttonSend.UseVisualStyleBackColor = true;
			this.buttonSend.Click += new System.EventHandler(this.buttonSend_Click);
			// 
			// listBoxUsers
			// 
			this.listBoxUsers.FormattingEnabled = true;
			this.listBoxUsers.ItemHeight = 16;
			this.listBoxUsers.Location = new System.Drawing.Point(11, 11);
			this.listBoxUsers.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.listBoxUsers.Name = "listBoxUsers";
			this.listBoxUsers.Size = new System.Drawing.Size(202, 404);
			this.listBoxUsers.TabIndex = 3;
			this.listBoxUsers.SelectedIndexChanged += new System.EventHandler(this.listBoxUsers_SelectedIndexChanged);
			this.listBoxUsers.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listBoxUsers_MouseDoubleClick);
			// 
			// labelUsersList
			// 
			this.labelUsersList.AutoSize = true;
			this.labelUsersList.Location = new System.Drawing.Point(11, 419);
			this.labelUsersList.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.labelUsersList.Name = "labelUsersList";
			this.labelUsersList.Size = new System.Drawing.Size(147, 17);
			this.labelUsersList.TabIndex = 4;
			this.labelUsersList.Text = "Пользователи в сети";
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(695, 444);
			this.Controls.Add(this.labelUsersList);
			this.Controls.Add(this.listBoxUsers);
			this.Controls.Add(this.buttonSend);
			this.Controls.Add(this.textBoxMessage);
			this.Controls.Add(this.textBoxChat);
			this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.Name = "MainForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Чат";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
			this.Load += new System.EventHandler(this.MainForm_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox textBoxChat;
		private System.Windows.Forms.TextBox textBoxMessage;
		private System.Windows.Forms.Button buttonSend;
		private System.Windows.Forms.ListBox listBoxUsers;
		private System.Windows.Forms.Label labelUsersList;
	}
}