﻿namespace SPExplorerThread
{
	partial class Spectator
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.pathLabel = new System.Windows.Forms.Label();
			this.filesCountLabel = new System.Windows.Forms.Label();
			this.filesSizeLabel = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// pathLabel
			// 
			this.pathLabel.AutoSize = true;
			this.pathLabel.Location = new System.Drawing.Point(16, 11);
			this.pathLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.pathLabel.Name = "pathLabel";
			this.pathLabel.Size = new System.Drawing.Size(36, 17);
			this.pathLabel.TabIndex = 0;
			this.pathLabel.Text = "path";
			// 
			// filesCountLabel
			// 
			this.filesCountLabel.AutoSize = true;
			this.filesCountLabel.Location = new System.Drawing.Point(16, 32);
			this.filesCountLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.filesCountLabel.Name = "filesCountLabel";
			this.filesCountLabel.Size = new System.Drawing.Size(71, 17);
			this.filesCountLabel.TabIndex = 1;
			this.filesCountLabel.Text = "Loading...";
			// 
			// filesSizeLabel
			// 
			this.filesSizeLabel.AutoSize = true;
			this.filesSizeLabel.Location = new System.Drawing.Point(16, 48);
			this.filesSizeLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.filesSizeLabel.Name = "filesSizeLabel";
			this.filesSizeLabel.Size = new System.Drawing.Size(71, 17);
			this.filesSizeLabel.TabIndex = 2;
			this.filesSizeLabel.Text = "Loading...";
			// 
			// Spectator
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(285, 84);
			this.Controls.Add(this.filesSizeLabel);
			this.Controls.Add(this.filesCountLabel);
			this.Controls.Add(this.pathLabel);
			this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
			this.Name = "Spectator";
			this.Text = "Spectator";
			this.Load += new System.EventHandler(this.Spectator_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label pathLabel;
		private System.Windows.Forms.Label filesCountLabel;
		private System.Windows.Forms.Label filesSizeLabel;
	}
}