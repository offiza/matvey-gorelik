﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
namespace Form3
{
	public partial class Form2 : Form
	{
		Thread t1;
		public Form2(Form1 form1)
		{
			t1 = new Thread(new ThreadStart(ThreadProc1));
			t1.Start();
			InitializeComponent();
		}

		private void label1_Click(object sender, EventArgs e)
		{

		}
		private void SetLabelText1Safe(string text)
		{
			if (InvokeRequired)
				BeginInvoke(new Action<string>(s => { SetLabelText1(s); }), text);
			else
				SetLabelText1(text);
		}
		public void SetLabelText1(string text)
		{
			label1.Text = text;
		}
		public void ThreadProc1()
		{
			int i = 0;
			while (i < 5)
			{
				i++;
				SetLabelText1Safe(i.ToString());
				Thread.Sleep(500);
			}
			this.BeginInvoke((MethodInvoker)(() => Close()));
			t1.Abort();
		}

		private void Form2_Load(object sender, EventArgs e)
		{

		}
	}
}
