﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;

namespace Processes
{
	public partial class ExeOpen : Form
	{
		public ExeOpen()
		{
			InitializeComponent();
			openFileDialogExe.Filter = " EXE files| *.exe";
		}

		private void buttonStart_Click(object sender, EventArgs e)
		{
			openFileDialogExe.ShowDialog();
		}
		
		private void openFileDialogExe_FileOk(object sender, CancelEventArgs e)
		{
			Process.Start(openFileDialogExe.FileName);

			if (e.Cancel)
			{
				return;
			}
		}
	}
}
