﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProcessesTxt
{
	public partial class OpenTxt : Form
	{
		public OpenTxt()
		{
			InitializeComponent();
		}

        private void button1_Click(object sender, EventArgs e)
        {
            openFileDialogTxt.ShowDialog();

        }

        private void openFileDialogTxt_FileOk(object sender, CancelEventArgs e)
        {
            if (e.Cancel)
            {
                return;
            }

            Process.Start(openFileDialogTxt.FileName);
        }
    }
}
