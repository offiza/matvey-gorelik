﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32;
namespace Registry2
{
	class Program
	{
		public const string KeyName = @"SOFTWARE\Matvey\MyApp2\RecentOpenedFiles";
		static void SetList(List<string> list)
		{
			try
			{
				using (RegistryKey key = Registry.CurrentUser.CreateSubKey(KeyName))
				{
					key.SetValue("Count", list.Count, RegistryValueKind.DWord);
					for (int i = 0; i < list.Count; i++)
						key.SetValue($"Item{i + 1}", list[i]);
					key.Close();
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
			}
		}
		static void GetCount()
		{
			using (RegistryKey key = Registry.CurrentUser.CreateSubKey(KeyName))
			{
				Console.WriteLine(key.GetValue("Count"));
				key.Close();
			}
		}
		static List<string> GetList()
		{
			var list = new List<string>();
			try
			{
				using (RegistryKey key = Registry.CurrentUser.OpenSubKey(KeyName))
				{
					int count = (int)key.GetValue("Count", RegistryValueKind.DWord);
					for (int i = 0; i < count; i++)
						list.Add(key.GetValue($"Item{i + 1}").ToString());
					key.Close();
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
			}
			return list;
		}
		static void Main(string[] args)
		{
			List<string> list =new List<string>()
			{
				@"string1",
				@"string2",
				@"string3",
				@"hello"
			};
			SetList(list);
			var list2 = GetList();
			foreach (var item in list2)
				Console.WriteLine(item);
			GetCount();
		}
	}
}
