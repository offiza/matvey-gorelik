﻿using System;
using Microsoft.Win32;

namespace ConsoleApp1
{
	class Program
	{
		static void Main(string[] args)
		{
			try
			{
				using (RegistryKey key = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\Matvey\MyApp1"))
				{
					key.SetValue("Time", $"{DateTime.UtcNow:dd.MM.yyyy HH:mm:ss}");
				}
				using (RegistryKey key = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Matvey\MyApp1"))
				{
					if (key != null && key.GetValue("Time") != null)
					{
						string time = key.GetValue("Time").ToString();
						Console.WriteLine($"Time : {time}");
					}
				}
				using (RegistryKey key = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Run"))
				{
					key.SetValue("Matvey", $"{AppDomain.CurrentDomain.BaseDirectory}{AppDomain.CurrentDomain.FriendlyName}");
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
			}
		}
	}
}
