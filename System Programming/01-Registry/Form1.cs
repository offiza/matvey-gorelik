﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
namespace WindowsFormsApp1
{

	
	public partial class Form1 : Form
	{
		int counter1;
		int counter2;
		int counter3;
		Thread t1;
		Thread t2;
		Thread t3;
		public Form1()
		{
			counter1 = 0;
			counter2 = 0;
			counter3 = 0;
			InitializeComponent();
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			
		}
		private void ThreadProc1()
		{
			while(true)
			{
				label1.Text = counter1.ToString();
				counter1++;
				Thread.Sleep(500);
			}
		}
		private void ThreadProc2()
		{
			while (true)
			{
				label2.Text = counter2.ToString();
				counter2++;
				Thread.Sleep(1000);
			}
		}
		private void ThreadProc3()
		{
			while (true)
			{
				label3.Text = counter3.ToString();
				counter3++;
				Thread.Sleep(1500);
			}
		}
		private void button_start_Click(object sender, EventArgs e)
		{
			t1 = new Thread(new ThreadStart(ThreadProc1));
			t2 = new Thread(new ThreadStart(ThreadProc2));
			t3 = new Thread(new ThreadStart(ThreadProc3));
			t1.Start();
			t2.Start();
			t3.Start();
		}

		private void button_stop_Click(object sender, EventArgs e)
		{
			t1.Abort();
			t2.Abort();
			t3.Abort();
		}

		private void button_reset_Click(object sender, EventArgs e)
		{
			counter1 = 0;
			counter2 = 0;
			counter3 = 0;
		}
	}
}
