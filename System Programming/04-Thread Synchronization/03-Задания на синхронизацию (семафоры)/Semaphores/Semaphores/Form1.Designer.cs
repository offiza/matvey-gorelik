﻿namespace Semaphores
{
	partial class Form1
	{
		/// <summary>
		/// Обязательная переменная конструктора.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Освободить все используемые ресурсы.
		/// </summary>
		/// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Код, автоматически созданный конструктором форм Windows

		/// <summary>
		/// Требуемый метод для поддержки конструктора — не изменяйте 
		/// содержимое этого метода с помощью редактора кода.
		/// </summary>
		private void InitializeComponent()
		{
			this.listBoxWorking = new System.Windows.Forms.ListBox();
			this.listBoxWaiting = new System.Windows.Forms.ListBox();
			this.listBoxCreating = new System.Windows.Forms.ListBox();
			this.labelWorking = new System.Windows.Forms.Label();
			this.labelWaiting = new System.Windows.Forms.Label();
			this.labelCreating = new System.Windows.Forms.Label();
			this.labelCount = new System.Windows.Forms.Label();
			this.numericUpDown = new System.Windows.Forms.NumericUpDown();
			this.buttonCreate = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown)).BeginInit();
			this.SuspendLayout();
			// 
			// listBoxWorking
			// 
			this.listBoxWorking.FormattingEnabled = true;
			this.listBoxWorking.ItemHeight = 16;
			this.listBoxWorking.Location = new System.Drawing.Point(12, 138);
			this.listBoxWorking.Name = "listBoxWorking";
			this.listBoxWorking.Size = new System.Drawing.Size(210, 84);
			this.listBoxWorking.TabIndex = 0;
			this.listBoxWorking.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listBoxWorking_MouseDoubleClick);
			// 
			// listBoxWaiting
			// 
			this.listBoxWaiting.FormattingEnabled = true;
			this.listBoxWaiting.ItemHeight = 16;
			this.listBoxWaiting.Location = new System.Drawing.Point(228, 138);
			this.listBoxWaiting.Name = "listBoxWaiting";
			this.listBoxWaiting.Size = new System.Drawing.Size(228, 84);
			this.listBoxWaiting.TabIndex = 0;
			// 
			// listBoxCreating
			// 
			this.listBoxCreating.FormattingEnabled = true;
			this.listBoxCreating.ItemHeight = 16;
			this.listBoxCreating.Location = new System.Drawing.Point(462, 138);
			this.listBoxCreating.Name = "listBoxCreating";
			this.listBoxCreating.Size = new System.Drawing.Size(227, 84);
			this.listBoxCreating.TabIndex = 0;
			this.listBoxCreating.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listBoxCreated_MouseDoubleClick);
			// 
			// labelWorking
			// 
			this.labelWorking.AutoSize = true;
			this.labelWorking.Location = new System.Drawing.Point(9, 106);
			this.labelWorking.Name = "labelWorking";
			this.labelWorking.Size = new System.Drawing.Size(60, 17);
			this.labelWorking.TabIndex = 1;
			this.labelWorking.Text = "Working";
			// 
			// labelWaiting
			// 
			this.labelWaiting.AutoSize = true;
			this.labelWaiting.Location = new System.Drawing.Point(225, 106);
			this.labelWaiting.Name = "labelWaiting";
			this.labelWaiting.Size = new System.Drawing.Size(55, 17);
			this.labelWaiting.TabIndex = 1;
			this.labelWaiting.Text = "Waiting";
			// 
			// labelCreating
			// 
			this.labelCreating.AutoSize = true;
			this.labelCreating.Location = new System.Drawing.Point(459, 106);
			this.labelCreating.Name = "labelCreating";
			this.labelCreating.Size = new System.Drawing.Size(61, 17);
			this.labelCreating.TabIndex = 1;
			this.labelCreating.Text = "Creating";
			// 
			// labelCount
			// 
			this.labelCount.AutoSize = true;
			this.labelCount.Location = new System.Drawing.Point(12, 259);
			this.labelCount.Name = "labelCount";
			this.labelCount.Size = new System.Drawing.Size(45, 17);
			this.labelCount.TabIndex = 1;
			this.labelCount.Text = "Count";
			// 
			// numericUpDown
			// 
			this.numericUpDown.Location = new System.Drawing.Point(12, 295);
			this.numericUpDown.Name = "numericUpDown";
			this.numericUpDown.Size = new System.Drawing.Size(120, 22);
			this.numericUpDown.TabIndex = 2;
			this.numericUpDown.ValueChanged += new System.EventHandler(this.numericUpDown_ValueChanged);
			// 
			// buttonCreate
			// 
			this.buttonCreate.Location = new System.Drawing.Point(228, 294);
			this.buttonCreate.Name = "buttonCreate";
			this.buttonCreate.Size = new System.Drawing.Size(228, 32);
			this.buttonCreate.TabIndex = 3;
			this.buttonCreate.Text = "Create";
			this.buttonCreate.UseVisualStyleBackColor = true;
			this.buttonCreate.Click += new System.EventHandler(this.buttonCreate_Click);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(701, 343);
			this.Controls.Add(this.buttonCreate);
			this.Controls.Add(this.numericUpDown);
			this.Controls.Add(this.labelCreating);
			this.Controls.Add(this.labelWaiting);
			this.Controls.Add(this.labelCount);
			this.Controls.Add(this.labelWorking);
			this.Controls.Add(this.listBoxCreating);
			this.Controls.Add(this.listBoxWaiting);
			this.Controls.Add(this.listBoxWorking);
			this.Name = "Form1";
			this.Text = "Form1";
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ListBox listBoxWorking;
		private System.Windows.Forms.ListBox listBoxWaiting;
		private System.Windows.Forms.ListBox listBoxCreating;
		private System.Windows.Forms.Label labelWorking;
		private System.Windows.Forms.Label labelWaiting;
		private System.Windows.Forms.Label labelCreating;
		private System.Windows.Forms.Label labelCount;
		private System.Windows.Forms.NumericUpDown numericUpDown;
		private System.Windows.Forms.Button buttonCreate;
	}
}

