﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Semaphores
{
	public partial class Form1 : Form
	{
		public static int count = 0;

		public int maxCount = 1;
		public int maxBCount =2;

		private static object lockObj = new object();

		private static Semaphore semaphore;

		public static List<ThreadObject> threadsCreating = new List<ThreadObject>();
		public static List<ThreadObject> threadsWaiting = new List<ThreadObject>();
		public static List<ThreadObject> threadsWorking = new List<ThreadObject>();
		public class ThreadObject
		{
			public int Index { get; set; } = 0;
			public int Count { get; set; } = 0;
			public Thread thread { get; set; }

			public ThreadObject()
			{
			}
		}
		public Form1()
		{
			InitializeComponent();
			numericUpDown.Value = 1;
		}

		private void buttonCreate_Click(object sender, EventArgs e)
		{
			ThreadObject newThreadObject = new ThreadObject();
			newThreadObject.thread = new Thread(new ThreadStart(DoWork));
			newThreadObject.thread.Name = $"Поток {++count}";

			threadsCreating.Add(newThreadObject);
			listBoxCreating.Items.Add(newThreadObject.thread.Name);
		}
		private void UpdateListBoxes()
		{
			listBoxCreating.Items.Clear();
			listBoxWaiting.Items.Clear();

			foreach (ThreadObject creating in threadsCreating)
				listBoxCreating.Items.Add(creating.thread.Name);
			foreach (ThreadObject waiting in threadsWaiting)
				listBoxWaiting.Items.Add(waiting.thread.Name);
		}
		public void DoWork()
		{
			semaphore.WaitOne();

			threadsWaiting.RemoveAt(0);

			listBoxWaiting.BeginInvoke((MethodInvoker)(() =>
			listBoxWaiting.Items.RemoveAt(0)));

			ThreadObject newObject = new ThreadObject
			{
				thread = Thread.CurrentThread,
				Index = listBoxWorking.Items.Count
			};

			threadsWorking.Add(newObject);

			listBoxWorking.BeginInvoke((MethodInvoker)(() =>
			listBoxWorking.Items.Add(newObject.thread.Name)));

			while (Thread.CurrentThread.IsAlive)
			{
				semaphore = new Semaphore(maxCount, maxCount);
				lock (lockObj)
				{
					listBoxWorking.BeginInvoke((MethodInvoker)(() =>
					listBoxWorking.Items[newObject.Index] = $"{newObject.thread.Name} - {++newObject.Count}"
					));
				}
				Thread.Sleep(500);
			}
		}
		private void UpdateWorkingThreadsIndexes()
		{
			lock (lockObj)
			{
				for (int i = 0; i < listBoxWorking.Items.Count; i++)
				{
					threadsWorking[i].Index = i;
				}
			}
		}
		private void numericUpDown_ValueChanged(object sender, EventArgs e)
		{
				maxCount = (int)numericUpDown.Value;
		}
		private void listBoxCreated_MouseDoubleClick(object sender, MouseEventArgs e)
		{
			if (listBoxCreating.SelectedIndex != -1)
			{
				threadsWaiting.Add(threadsCreating[listBoxCreating.SelectedIndex]);
				threadsCreating.RemoveAt(listBoxCreating.SelectedIndex);

				UpdateListBoxes();

				threadsWaiting[threadsWaiting.Count - 1].thread.Start();
			}
		}
		private void listBoxWorking_MouseDoubleClick(object sender, MouseEventArgs e)
		{
			if (listBoxWorking.SelectedIndex != -1)
			{
				threadsWorking[listBoxWorking.SelectedIndex].thread.Abort();

				threadsWorking.RemoveAt(listBoxWorking.SelectedIndex);
				listBoxWorking.Items.RemoveAt(listBoxWorking.SelectedIndex);

				UpdateWorkingThreadsIndexes();
				semaphore.Release();
			}
		}

	}
}
