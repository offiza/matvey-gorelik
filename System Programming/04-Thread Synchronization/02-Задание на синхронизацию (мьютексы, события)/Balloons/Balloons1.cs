﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace balloons
{
	class Program
	{
		private static Random random = new Random();
		private static object locker = new object();

		struct Coord
		{
			public int X { get; set; }
			public int Y { get; set; }

			public Coord(int x, int y)
			{
				X = x;
				Y = y;
			}
		}
		class Balloon
		{
			private bool checkCoord;
			private string name;
			public Thread thread;
			private Coord coord;

			public Balloon(string name)
			{
				thread = new Thread(new ThreadStart(Move));
				coord = new Coord();
				this.name = name;
				Create();
				thread.Start();
			}

			private void Create()
			{
				checkCoord = true;
				coord = new Coord(random.Next(0, 20), random.Next(0, 20));
				Console.WriteLine($"{name} baloon spawned at ({coord.X}; {coord.Y})");
			}

			private void CheckCoord()
			{
				if (coord.X > 30 || coord.Y > 30)
					checkCoord = false;
			}

			public void Move()
			{
				while (true)
				{
					if (checkCoord == true)
					{
						coord.X += random.Next(0, 2);
						coord.Y += random.Next(0, 2);
						Console.WriteLine($"{name} ballon is ({coord.X}; {coord.Y})");

						CheckCoord();
					}
					Thread.Sleep(300);
				}
			}
		}
		static void Main(string[] args)
		{
			Balloon balloon1 = new Balloon("Red");
			Balloon balloon2 = new Balloon("Yellow");
		}
	}
}
