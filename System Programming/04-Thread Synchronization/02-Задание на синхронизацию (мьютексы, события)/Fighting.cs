﻿using System;
using System.Threading;

namespace ComandsFight
{

	class Program
	{	
		public static object lockObj = new object();
		public static Random rand = new Random();
        public class Team
        {
            public Thread thread;
            private Team enemy;
            public int count;
            public string name;
            public string Name { get => name; private set => name = value; }

            public Team(string name, int count)
            {
                this.count = count;
                Name = name;
            }
            public void SetEnemy(Team team)
            {
                enemy = team;
            }
            public void Fight()
            {
                while (this.count > 0)
                {
                    lock (lockObj)
                    {
                        int Killed = rand.Next(0, this.count);
                        enemy.count -= Killed;
                        Console.WriteLine($"{Killed} воинов было убито из {enemy.name}");
                        Thread.Sleep(rand.Next(300, 900));

                        int Died = rand.Next(0, Killed);
                        this.count -= Died;
                        Console.WriteLine($"{Died} воинов было убито из {name}");
                        Thread.Sleep(rand.Next(300, 900));

                        int Revived = rand.Next(0, 10);
                        this.count += Revived;
                        Console.WriteLine($"{Revived} воинов добавилось в ряды сражений");
                    }
                    Thread.Sleep(rand.Next(300, 900));
                }
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine($"<!> {enemy.count}");

                Console.WriteLine($"<!> {this.count}");


                enemy.thread.Abort();
                this.thread.Abort();
            }
        }
        public class Arena
        {
            Team team1 = new Team("Команда 1",20);
            Team team2 = new Team("Команда 2",20);
            Team team3 = new Team("Команда 3",20);

            public void Start()
            {
                team2.SetEnemy(team1);
                team2.SetEnemy(team3);
                team1.SetEnemy(team2);
                team3.SetEnemy(team1);

                team1.thread = new Thread(new ThreadStart(team1.Fight));
                team2.thread = new Thread(new ThreadStart(team2.Fight));
                team3.thread = new Thread(new ThreadStart(team3.Fight));

                team1.thread.Start();
                team2.thread.Start();
                team3.thread.Start();
            }
        }
        static void Main(string[] args)
		{
            Arena arena = new Arena();
            arena.Start();
        }
	}
}
