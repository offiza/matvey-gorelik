﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Puh
{
	class Program
	{
		public const int maxEat = 1500;
		public const int minEat = 1000;
		public const int bearMaxCount = 10;
		public const int bearMinCount = 5;
		public const int minTimeCreateHoney = 500;
		public const int maxTimeCreateHoney = 2000;
		public const int minCreateHoney = 1;
		public const int maxCreateHoney = 6;
		public const int bees = 2;
		private static Random random = new Random();
		private static int honeyCount = 20;
		private static object lockObj = new object();		
		public class Bee
		{
			private Thread thread;
			public Bee()
			{
				thread = new Thread(new ThreadStart(AddHoney));
				thread.Start();
			}
			public void AddHoney()
			{
				while (true)
				{
					int sleep_value = random.Next(minTimeCreateHoney, maxTimeCreateHoney);
					int honeyCountBee = random.Next(minCreateHoney, maxCreateHoney);

					Thread.Sleep(sleep_value);

					lock (lockObj)
						honeyCount += honeyCountBee;
					Console.WriteLine($"Пчела принесла {honeyCountBee} мёда");
					Show();
				}
			}
		}

		public class Bear
		{
			private bool isAlive;
			private int countEatHoney;
			private Thread thread;
			public Bear()
			{
				isAlive = true;
				thread = new Thread(new ThreadStart(EatHoney));
				thread.Start();
			}
			private void Live()
			{
				if (honeyCount < countEatHoney)
				{
					Console.WriteLine($"Винии Пух умер");
					isAlive = false;
				}
			}
			public void EatHoney()
			{
				while (isAlive)
				{
					lock (lockObj)
					{
						countEatHoney = random.Next(bearMinCount, bearMaxCount);
						honeyCount -= countEatHoney;
					}
					Thread.Sleep(random.Next(minEat, maxEat));
					Show();
					Console.WriteLine($"Винии Пух съел {countEatHoney} мёда");
					Console.ResetColor();
					Live();
				}
			}
		}

		public static void Show()
		{
			lock (lockObj)
			{

				Console.WriteLine($"Количество мёда: {honeyCount}");
			}
		}

		static void Main(string[] args)
		{
			List<Bee> Bees = new List<Bee>();
			for (int i = 0; i < bees; i++)
				Bees.Add(new Bee());

			Bear Puh = new Bear();
		}
	}
}
