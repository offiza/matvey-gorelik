﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Airplanes
{
	class Program
	{
		public static Random rand = new Random();
		public static Stopwatch watch = new Stopwatch();
		public static object lockObj = new object();

		const int time = 10;
		const int width = 20;
		const int height = 20;

        public class Scout
        {
            public char ch = 'S';
            int x;
            int y;
            public Thread thread;
            public Scout()
            {
                x = rand.Next(0, 19);
                y = rand.Next(0, 19);
            }
            public void Move()
            {
                int value = rand.Next(0, 3);
                if (value == 0)
                {
                    if (y != 0)
                        y--;
                    else Move();
                }
                else if (value == 1)
                {
                    if (y != 19)
                        y++;
                    else Move();
                }
                if (value == 2)
                {
                    if (x != 19)
                        x++;
                    else Move();
                }
                else
                {
                    if (x != 0)
                        x--;
                    else Move();
                }
            }
            public void Attack(object _board)
            {
                while (watch.ElapsedMilliseconds <= time * 1000)
                {
                    lock (lockObj)
                    {
                        char[,] __board = (char[,])_board;
                        Move();
                        if (__board[x, y] == '1')
                            found++;
                        __board[x, y] = ch;

                        Show(__board);
                    }
                    Thread.Sleep(rand.Next(300, 500));
                }
                thread.Abort();
            }
            private void Show(char[,] _board)
            {
                Console.Clear();
                for (int i = 0; i < height; i++)
                {
                    Console.WriteLine();
                    for (int j = 0; j < width; j++)
                    {
                        Console.Write(_board[i, j]);
                    }
                }
            }
            public int found { get; private set; } = 0;
        }

        public class Region
		{
            public Scout[] scouts = new Scout[10];
            public static char[,] board = new char[width, height];
            public Region()
            {
                for (int i = 0; i < 10; i++)
                    scouts[i] = new Scout();

                Initialize();
                Start();
            }

            private void Initialize()
            {
                for (int i = 0; i < height; i++)
                {
                    for (int j = 0; j < width; j++)
                    {
                        int value = rand.Next(0, 2);
                        if (value == 0)
                            board[i, j] = '0';
                        else
                            board[i, j] = '1';
                    }
                }
            }

            public void Start()
            {
                watch.Start();
                int value = 1;
                foreach (var scout in scouts)
                {
                    scout.thread = new Thread(new ParameterizedThreadStart(scout.Attack));
                    scout.thread.Name = $"Scout #{value}";
                    scout.thread.Start(board);
                    value+=1;
                }
            }
        }

		static void Main(string[] args)
		{
            Region region = new Region();

            Console.ReadKey();

            foreach (var scout in region.scouts)
                Console.WriteLine($"{scout.thread.Name} - {scout.found}");
        }
	}
}
