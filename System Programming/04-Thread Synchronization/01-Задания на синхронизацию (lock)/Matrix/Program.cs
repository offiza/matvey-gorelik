﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Matrix
{
	class Program
	{
		private static Random rand = new Random();
		private static char[] CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()1234567890;:|'".ToCharArray();
		private const int maxLength = 8;
		private const int minLength = 3;
		private const int maxSleep = 300;
		private const int minSleep = 100;
		private static object lockObject = new object();

		public struct MatrixChar
		{
			public int x, y;
			public char Char { get; set; }
		}

		public class MatrixString
		{
			private int sleep;
			private MatrixChar[] chars;
			private int X = 0;
			private Thread thread;

			public MatrixString()
			{
				X = rand.Next(0, 40);
				GenerateString();
				sleep = rand.Next(minSleep, maxSleep);
				thread = new Thread(new ThreadStart(Move));
				thread.IsBackground = false;
				thread.Start();
			}

			private void GenerateString()
			{
				chars = new MatrixChar[rand.Next(minLength, maxLength)];
				for (int i = 0; i < chars.Length; i++)
				{
					chars[i].Char = CHARS[rand.Next(0, CHARS.Length)]; 
					chars[i].x = X;
					chars[i].y = i;
				}
			}

			public void Move()
			{
				while (true)
				{
					lock (lockObject)
					{
						for (int i = 0; i < chars.Length; i++)
						{
							Console.SetCursorPosition(chars[i].x, chars[i].y);
							Console.Write(" ");
						}

						for (int i = 0; i < chars.Length; i++)
						{
							if (i == chars.Length - 1)
								Console.ForegroundColor = ConsoleColor.White;
							else if (i == chars.Length - 2)
								Console.ForegroundColor = ConsoleColor.Magenta;
							else
								Console.ForegroundColor = ConsoleColor.DarkMagenta;
							if (chars[chars.Length - 1].y >= 30)
								GenerateString();
							Console.SetCursorPosition(chars[i].x, ++chars[i].y);
							Console.Write(chars[i].Char);
						}
					}
					Thread.Sleep(sleep);
				}
			}
		}

		static void Main(string[] args)
		{
			Console.SetWindowSize(100, 50);
			Console.CursorVisible = false;
			List<MatrixString> strs = new List<MatrixString>();
			for (int i = 0; i < 40; i++)
				strs.Add(new MatrixString());
		}
	}
}
