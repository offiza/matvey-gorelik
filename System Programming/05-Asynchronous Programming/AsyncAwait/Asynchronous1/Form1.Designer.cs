﻿namespace Asynchronous1
{
	partial class Form1
	{
		/// <summary>
		/// Обязательная переменная конструктора.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Освободить все используемые ресурсы.
		/// </summary>
		/// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Код, автоматически созданный конструктором форм Windows

		/// <summary>
		/// Требуемый метод для поддержки конструктора — не изменяйте 
		/// содержимое этого метода с помощью редактора кода.
		/// </summary>
		private void InitializeComponent()
		{
			this.labelFrom = new System.Windows.Forms.Label();
			this.labelTo = new System.Windows.Forms.Label();
			this.textBoxFrom = new System.Windows.Forms.TextBox();
			this.textBoxTo = new System.Windows.Forms.TextBox();
			this.buttonFrom = new System.Windows.Forms.Button();
			this.buttonTo = new System.Windows.Forms.Button();
			this.progressBar1 = new System.Windows.Forms.ProgressBar();
			this.buttonCopy = new System.Windows.Forms.Button();
			this.openFileDialogFrom = new System.Windows.Forms.OpenFileDialog();
			this.openFileDialogTo = new System.Windows.Forms.OpenFileDialog();
			this.SuspendLayout();
			// 
			// labelFrom
			// 
			this.labelFrom.AutoSize = true;
			this.labelFrom.Location = new System.Drawing.Point(12, 15);
			this.labelFrom.Name = "labelFrom";
			this.labelFrom.Size = new System.Drawing.Size(60, 17);
			this.labelFrom.TabIndex = 0;
			this.labelFrom.Text = "От куда";
			// 
			// labelTo
			// 
			this.labelTo.AutoSize = true;
			this.labelTo.Location = new System.Drawing.Point(12, 47);
			this.labelTo.Name = "labelTo";
			this.labelTo.Size = new System.Drawing.Size(40, 17);
			this.labelTo.TabIndex = 1;
			this.labelTo.Text = "Куда";
			// 
			// textBoxFrom
			// 
			this.textBoxFrom.Location = new System.Drawing.Point(78, 12);
			this.textBoxFrom.Name = "textBoxFrom";
			this.textBoxFrom.Size = new System.Drawing.Size(373, 22);
			this.textBoxFrom.TabIndex = 2;
			// 
			// textBoxTo
			// 
			this.textBoxTo.Location = new System.Drawing.Point(78, 47);
			this.textBoxTo.Name = "textBoxTo";
			this.textBoxTo.Size = new System.Drawing.Size(373, 22);
			this.textBoxTo.TabIndex = 2;
			// 
			// buttonFrom
			// 
			this.buttonFrom.Location = new System.Drawing.Point(464, 11);
			this.buttonFrom.Name = "buttonFrom";
			this.buttonFrom.Size = new System.Drawing.Size(94, 23);
			this.buttonFrom.TabIndex = 3;
			this.buttonFrom.Text = "Файл...";
			this.buttonFrom.UseVisualStyleBackColor = true;
			this.buttonFrom.Click += new System.EventHandler(this.buttonFrom_Click);
			// 
			// buttonTo
			// 
			this.buttonTo.Location = new System.Drawing.Point(464, 46);
			this.buttonTo.Name = "buttonTo";
			this.buttonTo.Size = new System.Drawing.Size(94, 23);
			this.buttonTo.TabIndex = 3;
			this.buttonTo.Text = "Файл...";
			this.buttonTo.UseVisualStyleBackColor = true;
			this.buttonTo.Click += new System.EventHandler(this.buttonTo_Click);
			// 
			// progressBar1
			// 
			this.progressBar1.Location = new System.Drawing.Point(15, 84);
			this.progressBar1.Name = "progressBar1";
			this.progressBar1.Size = new System.Drawing.Size(436, 23);
			this.progressBar1.TabIndex = 4;
			// 
			// buttonCopy
			// 
			this.buttonCopy.Location = new System.Drawing.Point(464, 84);
			this.buttonCopy.Name = "buttonCopy";
			this.buttonCopy.Size = new System.Drawing.Size(94, 23);
			this.buttonCopy.TabIndex = 3;
			this.buttonCopy.Text = "Копировать";
			this.buttonCopy.UseVisualStyleBackColor = true;
			this.buttonCopy.Click += new System.EventHandler(this.buttonCopy_Click);
			// 
			// openFileDialogFrom
			// 
			this.openFileDialogFrom.FileName = "openFileDialog1";
			// 
			// openFileDialogTo
			// 
			this.openFileDialogTo.FileName = "openFileDialog1";
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(577, 141);
			this.Controls.Add(this.progressBar1);
			this.Controls.Add(this.buttonCopy);
			this.Controls.Add(this.buttonTo);
			this.Controls.Add(this.buttonFrom);
			this.Controls.Add(this.textBoxTo);
			this.Controls.Add(this.textBoxFrom);
			this.Controls.Add(this.labelTo);
			this.Controls.Add(this.labelFrom);
			this.Name = "Form1";
			this.Text = "Form1";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label labelFrom;
		private System.Windows.Forms.Label labelTo;
		private System.Windows.Forms.TextBox textBoxFrom;
		private System.Windows.Forms.TextBox textBoxTo;
		private System.Windows.Forms.Button buttonFrom;
		private System.Windows.Forms.Button buttonTo;
		private System.Windows.Forms.ProgressBar progressBar1;
		private System.Windows.Forms.Button buttonCopy;
		private System.Windows.Forms.OpenFileDialog openFileDialogFrom;
		private System.Windows.Forms.OpenFileDialog openFileDialogTo;
	}
}

