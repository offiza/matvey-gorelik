﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Asynchronous1
{
	public partial class Form1 : Form
	{
		const int bufferSize = 4096;

		private delegate void FileCopierHandler(Form form, OpenFileDialog openFileDialogFrom, OpenFileDialog openFileDialogTo, ProgressBar progressBar);

		static void CopyFile(Form form, OpenFileDialog openFileDialogFrom, OpenFileDialog openFileDialogTo, ProgressBar progressBar)
		{
			double oneBlockPercent;
			double progress = 0d;

			using (var sourceFileStream = File.OpenRead(openFileDialogFrom.FileName))
			{
				oneBlockPercent = 100d / (sourceFileStream.Length / bufferSize);

				using (var streamReader = new StreamReader(sourceFileStream, Encoding.UTF8, true, bufferSize))
				{
					using (var streamWriter = new StreamWriter(openFileDialogTo.FileName, false, Encoding.UTF8, bufferSize))
					{
						char[] buffer = new char[bufferSize];

						int len = 0;
						int blocks = 0;

						while ((len = streamReader.ReadBlock(buffer, 0, bufferSize)) != 0)
						{
							blocks++;

							streamWriter.Write(buffer, 0, len);

							form.Text = $"{len} {blocks}";

							if (progress + oneBlockPercent > 100)
							{
								progress = 100;
							}
							else
							{
								progress += oneBlockPercent;
							}

							progressBar.Value = (int)progress;

							Thread.Sleep(100);
						}
					}
				}
			}
		}

		static async void CopyFileHandler(Form form, OpenFileDialog openSourceFile, OpenFileDialog openDestinationFile, ProgressBar progressBar)
		{
			await Task.Run(() => CopyFile(form, openSourceFile, openDestinationFile, progressBar));
		}

		public Form1()
		{
			InitializeComponent();
		}

		private void buttonFrom_Click(object sender, EventArgs e)
		{
			openFileDialogFrom.ShowDialog();
			textBoxFrom.Text = openFileDialogFrom.FileName;
		}

		private void buttonTo_Click(object sender, EventArgs e)
		{
			openFileDialogTo.ShowDialog();
			textBoxTo.Text = openFileDialogTo.FileName;
		}

		private void buttonCopy_Click(object sender, EventArgs e)
		{
			CopyFileHandler(this, openFileDialogFrom, openFileDialogTo, progressBar1);
		}

	}
}
