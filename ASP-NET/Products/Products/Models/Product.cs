using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Products
{
    public class Product
    {
        string Name { get; set; }
        string About { get; set; }
        string Price { get; set; }
    }
}
