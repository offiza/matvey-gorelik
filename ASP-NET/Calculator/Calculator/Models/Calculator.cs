using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Calculator
{
    public class Calculator
    {
        public string Calculate(double first, double second, string action)
        {
            try
            {
                if (action == "+")
                    return (first + second).ToString();
                else if (action == "-")
                    return (first - second).ToString();
                else if (action == "*")
                    return (first * second).ToString();
                else if (action == "/")
                    return (first / second).ToString();
                else
                    return "pizza";            }
            catch
            {
                return "ERROR";
            }
        }
    }
}
